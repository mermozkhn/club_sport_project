package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Utilisateur;

import java.io.IOException;

import dao.EvenementDAO;

/**
 * Servlet implementation class LikeServlet
 */
public class LikeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LikeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 HttpSession session = request.getSession(false);
		 if(session != null) {
			 Utilisateur utilisateur = (Utilisateur) session.getAttribute("userName");
			 
			  if (utilisateur == null) {
		           response.getWriter().write("{\"success\": false, \"message\": \"Vous devez être connecté pour liker.\"}");
		           return;
		        }
			  else {
				  String pos = utilisateur.getprofil();
				  if(!pos.isEmpty() && pos.equals("Lambda")) {
					  int idUtilisateur = utilisateur.getId_user();
				        int IdEvenement= Integer.parseInt(request.getParameter("IdEvenement"));

				        System.out.println("User ID: " + idUtilisateur + " is liking event ID: " + IdEvenement);

				        EvenementDAO evenementDAO = new EvenementDAO();
				        boolean success = evenementDAO.likeEvent(idUtilisateur, IdEvenement);

				        response.setContentType("application/json");
				        if (success) {
				            int likeCount = evenementDAO.getLikeCount(IdEvenement);
				            response.getWriter().write("{\"success\": true, \"likes\": " + likeCount + "}");
				        } else {
				            boolean alreadyLiked = evenementDAO.hasLikedEvent(idUtilisateur, IdEvenement);
				            if (alreadyLiked) {
				            	boolean to = evenementDAO.likeDelete(idUtilisateur, IdEvenement);
				            	response.setContentType("application/json");
				            	if(to==true) {
				            		int likeCount = evenementDAO.getLikeCount(IdEvenement);
						            response.getWriter().write("{\"success\": true, \"likes\": " + likeCount + "}");
				            	}else {
				            		 response.getWriter().write("{\"success\": false, \"message\": \"Erreur lors de l'ajout du like.\"}");
								        
				            	}
				            	
				            } 
				        else {
				            response.getWriter().write("{\"success\": false, \"message\": \"Erreur lors de l'ajout du like.\"}");
				        }
				        }
				  }
			  }
		 }
	}

}
