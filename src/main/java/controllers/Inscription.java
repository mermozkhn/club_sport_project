package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.owasp.encoder.Encode;
import java.io.IOException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;

import dao.*;
import model.*;
import jakarta.servlet.http.Part;


/**
 * Implémentation de la servlet pour le processus d'inscription.
 */
@WebServlet("/inscription")
@MultipartConfig(
    fileSizeThreshold = 1024 * 1024 * 2,  // 2MB
    maxFileSize = 1024 * 1024 * 10,       // 10MB
    maxRequestSize = 1024 * 1024 * 50      // 50MB
)
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Inscription() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Récupération des données du formulaire.
        request.setCharacterEncoding("UTF-8"); // Assurez-vous que la requête est interprétée comme étant en UTF-8
         
        // Récupération des données du formulaire.
        String nom = Encode.forHtml(request.getParameter("nom")); // Pour les champs de texte
        String prenom = Encode.forHtml(request.getParameter("prenom"));
        String email = Encode.forHtml(request.getParameter("email"));
        String motDePasse = Encode.forHtml(request.getParameter("motDePasse")); // Assurez-vous que l'attribut 'name' du champ mot de passe correspond
        String profil = Encode.forHtml(request.getParameter("fonction"));
        String dom = Encode.forHtml(request.getParameter("critere"));
        String fed = Encode.forHtml(request.getParameter("fede"));
        String region= Encode.forHtml(request.getParameter("region"));
        
        HttpSession sessi = request.getSession(false);
        if(sessi != null) {
        	Utilisateur nome=(Utilisateur) sessi.getAttribute("userName");
        	
        	if(nome!= null) {
        		response.sendRedirect("Logout");
        	}
        	else{
        		 // Vérification et hashage du mot de passe
                String hashedPassword = null;
                if ((motDePasse != null && !motDePasse.isEmpty()) || (profil != null && !profil.isEmpty())) {
                    hashedPassword = hashPassword(motDePasse);
                } else {
                    // Gérer le cas où le mot de passe est nul ou vide
                    request.setAttribute("errorMessage", "Le mot de passe est requis.");
                    request.getRequestDispatcher("Inscription.jsp").forward(request, response);
                    return;
                }

             // Récupération du fichier
                Part filePart = request.getPart("file-upload");
                String fileName = null;
                InputStream fileContent = null;

                if (filePart != null) {
                    fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
                    fileContent = filePart.getInputStream();
                }
            
            
                if(profil.equals("Elus")) {
                	
                	 // Vérifiez si le type de contenu du fichier est un PDF
                    if (!filePart.getContentType().equals("application/pdf")) {
                    	request.setAttribute("errorMessage", "Inscription échouée, veuillez réessayer en rentrant un PDF.");
                        request.getRequestDispatcher("Inscription.jsp").forward(request, response);
        	            return;
                    }

                    // Limiter la taille du fichier (par exemple, 10 Mo)
                    if (filePart.getSize() > 10 * 1024 * 1024) {
                    	request.setAttribute("errorMessage", "Inscription échouée, veuillez réessayer en choisissant un PDF inferieur a 10 Mo.");
                        request.getRequestDispatcher("Inscription.jsp").forward(request, response);
        	            return;
                    }
                    	
                	  // Création de l'utilisateur
                    usersDAO userDao = new usersDAO();
                    boolean isUserCreated = userDao.addUser(nom, prenom, email, hashedPassword, profil, fileName, fileContent,region,"");
                    if (isUserCreated) {
                    	request.getRequestDispatcher("Connexion.jsp").forward(request, response);
                    } else {
                        request.setAttribute("errorMessage", "Inscription échouée, veuillez réessayer.");
                        request.getRequestDispatcher("Inscription.jsp").forward(request, response);
                    }

                }
                if(profil.equals("Lambda"))  {
                	usersDAO userDao = new usersDAO();
      	            boolean isUserCreated = userDao.addUse(nom, prenom, email, hashedPassword, profil, "Nothing", fileContent,"Nothing","Nothing");

      	            
      	            if (isUserCreated) {
      	            	request.getRequestDispatcher("Connexion.jsp").forward(request, response);
      	            } else {
      	                request.setAttribute("errorMessage", "Inscription échouée, veuillez réessayer.");
      	                request.getRequestDispatcher("Inscription.jsp").forward(request, response);
      	            }
                }
                else{
                	ClubDAO db = new ClubDAO();
                    List<String> list = db.findByPostal(dom);
                    List<Clubs> club = new ArrayList<>();
                    if (list.isEmpty()) {
            		    request.setAttribute("erreur", "Il n'y a pas de commune avec ce code postal !");
            		    request.getRequestDispatcher("Inscription.jsp").forward(request, response);
            		} else {
            			for (String lis : list) {
        		            Clubs cu = db.selFilterCommune(lis, fed);
        		            if (cu != null && cu.getNom_federation() != null) {
        		                club.add(cu);
        		            }
        		        }
        		        List<Commune> com = new ArrayList<>();
        		        for (Clubs cl : club) {
        		            Commune comm = db.keepCommune(cl.getCode_commune(), fed);
        		            if (comm != null && comm.getLabel() != null) {
        		                com.add(comm);
        		            }
        		        }
        		        System.out.println(com.toString());
        		        if (com.isEmpty()) {
        		            request.setAttribute("erreur", "Ce code postal dispose d'une Commune mais ne dispose pas de club pour cette Fédération !");
        		        }
        		        
        		        // Création de l'utilisateur
        	            usersDAO userDao = new usersDAO();
        	            boolean isUserCreated = userDao.addUser(nom, prenom, email, hashedPassword, profil, fileName, fileContent,com.get(0).getNomCommune(),fed);

        	            
        	            if (isUserCreated) {
        	            	request.getRequestDispatcher("Connexion.jsp").forward(request, response);
        	            } else {
        	                request.setAttribute("errorMessage", "Inscription échouée, veuillez réessayer.");
        	                request.getRequestDispatcher("Inscription.jsp").forward(request, response);
        	            }
            		}
                   
                }
          
        	}
        }
       
    }
    
	 // Méthode pour hasher un mot de passe
    public static String hashPassword(String plainPassword) {
        // Le paramètre 12 est le coût de génération, plus il est élevé, plus c'est sûr mais lent
        return BCrypt.hashpw(plainPassword, BCrypt.gensalt(12));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("Inscription.jsp").forward(request, response);
    }

}
