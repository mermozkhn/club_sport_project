package controllers;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import dao.*;
import model.*;
import org.owasp.encoder.Encode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class Traitement
 */
public class Traitement extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Traitement() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	response.sendRedirect("Logout");
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String code_fede =Encode.forHtml(request.getParameter("federation"));
		String region=Encode.forHtml(request.getParameter("region"));
		String rayon = Encode.forHtml(request.getParameter("rayonn"));
		String select = Encode.forHtml(request.getParameter("type_recherche"));
		String postal =Encode.forHtml(request.getParameter("critere"));
		ClubDAO db = new ClubDAO();
		
		
		if(code_fede.isEmpty() && region.isEmpty() && rayon.isEmpty() && postal.isEmpty()) {
			response.sendRedirect("Logout");
		} else if (select.equals("code_postal")) {
			List<String> list = db.findByPostal(postal);
			if (list.isEmpty()) {
			    request.setAttribute("erreur", "Il n'y a pas de commune avec ce code postal !");
			} else {
			    List<Clubs> club = new ArrayList<>();
			    if (code_fede.equals("30")) {
			    	Seeker.writelog(request, select, postal, "Toutes les Federations");
			        List<Commune> com = new ArrayList<>();
			        for (String lis : list) {
			            List<Clubs> cu = db.selFilterCommuneAlone(lis);
			            if (cu != null && !cu.isEmpty()) {
			                club.addAll(cu);
			                for (Clubs clu : cu) {
			                    Commune comm = db.keepCommuneAlone(clu.getCode_commune());
			                    if (comm != null) {
			                        com.add(comm);
			                    }
			                }
			            }
			        }
			        if (com.isEmpty()) {
			            request.setAttribute("erreur", "Ce code postal dispose d'une Commune mais ne dispose pas de club pour cette Fédération !");
			        } else {
			            request.setAttribute("clubs", club);
			            request.setAttribute("communes", com);
			        }
			    } else {
			    	Seeker.writelog(request, select, postal, db.fed(code_fede));
			        for (String lis : list) {
			            Clubs cu = db.selFilterCommune(lis, code_fede);
			            if (cu != null && cu.getNom_federation() != null) {
			                club.add(cu);
			            }
			        }
			        List<Commune> com = new ArrayList<>();
			        for (Clubs cl : club) {
			            Commune comm = db.keepCommune(cl.getCode_commune(), code_fede);
			            if (comm != null && comm.getLabel() != null) {
			                com.add(comm);
			            }
			        }
			        if (com.isEmpty()) {
			            request.setAttribute("erreur", "Ce code postal dispose d'une Commune mais ne dispose pas de club pour cette Fédération !");
			        } else {
			            request.setAttribute("clubs", club);
			            request.setAttribute("communes", com);
			        }
			    }
			}

		} else if (select.equals("region")) {
			List<Clubs> club = new ArrayList<>();
			ArrayList<Commune> com = new ArrayList<>();
			if(code_fede.equals("30")) {
				Seeker.writelog(request, select, region, "Toutes les Federations");
				club=db.selFilterRegionAlone(region);
				for(Clubs clu : club) {
					Commune comm = new Commune();
					comm=db.keepCommuneAlone(clu.getCode_commune());
					if(comm != null) {
						com.add(comm);
					}
				}
			}else {
				Seeker.writelog(request, select, region, db.fed(code_fede));
				club=db.selFilterRegion(code_fede,region);
				for(Clubs clu : club) {
					Commune comm = new Commune();
					comm=db.keepCommune(clu.getCode_commune(),code_fede);
					if(comm != null) {
						com.add(comm);
					}
				}
			}
			if(com.size()==0) {
				//gerer une erreur pour dire qu'il n'y a pas de club au niveau de celui ci
				request.setAttribute("erreur","Concernant cette Federation il ny a pas de club dans cette region");
			}
			else {
				request.setAttribute("clubs", club);
				request.setAttribute("communes", com);
			}
		} 
		else if (select.equals("rayon")) {
		     Double latitudeUser = Double.valueOf(request.getParameter("latitudeUser"));
		     Double longitudeUser = Double.valueOf(request.getParameter("longitudeUser"));
		     Double rayo = Double.valueOf(rayon);
		     List<Commune> list = db.selectByRayon(rayo, latitudeUser, longitudeUser);
		     if (list.isEmpty()) {
		         request.setAttribute("erreur", "Il n'y a pas de commune près de vous avec ce rayon !");
		     } else {
		         List<Clubs> club = new ArrayList<>();
		         
		         if (code_fede.equals("30")) {
		        	 Seeker.writelog(request, select, rayon + "km", "Toutes les Federations");
		             List<Commune> com = new ArrayList<>();
		             
		             for (Commune lis : list) {
		                 List<Clubs> cu = db.selFilterCommuneAlone(lis.getInseeCode());
		                 if (cu != null && !cu.isEmpty()) {
		                     club.addAll(cu); // Ajoutez tous les éléments de 'cu' à 'club'
		                 }
		                 
		             }
		             for (Clubs clu : club) {
	                     Commune comm = db.keepCommuneAlone(clu.getCode_commune());
	                     if (comm != null) {
	                         com.add(comm);
	                     }
	                 }
		             
		             if (com.isEmpty()) {
		                 // Gérer l'erreur lorsque la liste des communes est vide
		                 request.setAttribute("erreur", "Ce code postal dispose d'une Commune mais ne dispose pas de club pour cette Fédération !");
		             } else {
		            	 System.out.println("com : "+com.size());
		            	 System.out.println("club : "+club.size());
		                 request.setAttribute("clubs", club);
		                 request.setAttribute("communes", com);
		             }
		             
		         } else {
		        	 Seeker.writelog(request, select, rayon + "km", db.fed(code_fede));
		             for (Commune lis : list) {
		                 Clubs cu = db.selFilterCommune(lis.getInseeCode(), code_fede);
		                 if (cu != null && cu.getNom_federation() != null) {
		                     club.add(cu);
		                 }
		             }
		             
		             List<Commune> com = new ArrayList<>();
		             for (Clubs cl : club) {
		                 Commune comm = db.keepCommune(cl.getCode_commune(), code_fede);
		                 if (comm != null && comm.getLabel() != null) {
		                     com.add(comm);
		                 }
		             }
		             
		             if (com.isEmpty()) {
		                 // Gérer l'erreur lorsque la liste des communes est vide
		                 request.setAttribute("erreur", "Ce code postal dispose d'une Commune mais ne dispose pas de club pour cette Fédération !");
		             } else {
		                 request.setAttribute("clubs", club);
		                 request.setAttribute("communes", com);
		                 request.setAttribute("rayon", rayon);
		             }
		         }
		     }

			 
		} 
		RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
		dispatcher.forward(request, response);
	}

}
