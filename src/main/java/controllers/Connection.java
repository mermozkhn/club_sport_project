package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.owasp.encoder.Encode;
import model.Logger;
import model.Utilisateur;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import dao.*;

/**
 * Servlet implementation class Connection
 */
public class Connection extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Connection() {
        super();
        // TODO Auto-generated constructor stub
    }

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			String email = Encode.forHtml(request.getParameter("email"));
	        String motDePasse = Encode.forHtml(request.getParameter("password"));
	        String fonction = Encode.forHtml(request.getParameter("fonction"));
	        HttpSession sessi = request.getSession(false);

	        if(sessi != null) {
	        	Utilisateur nome=(Utilisateur) sessi.getAttribute("userName");
	        	
	        	if(nome!= null) {
	        		response.sendRedirect("Logout");
	        	}
	        	else{
	        		
	      
	        // Création d'une instance DAO pour interagir avec la base de données.
	        usersDAO userDao = new usersDAO();
	        
	        // Vérification de l'authenticité de l'utilisateur.
	        Utilisateur userName = userDao.authenticateUser(email, motDePasse);

	      if ((userName != null) ) {
	            HttpSession session = request.getSession();
	            session.setAttribute("userName", userName);  // Ajouter le nom de l'utilisateur à la session
	            Logger.writelog(request, "Succes");
	            response.sendRedirect("index.jsp"); // Redirige vers la page d'accueil

	        } else {
	            request.setAttribute("errorMessage", "Connexion Echouee veuillez rentrer les bons identifiants.");
	            Logger.writelog(request, "Echouee");
	            request.getRequestDispatcher("Connexion.jsp").forward(request, response);
	        }
	      
	        	}
	        	
	        
	        }

	    }
	    // Méthode pour gérer les requêtes GET, qui redirige vers la page de connexion.
	    @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    	Logger.writelog(request, "Echouee");
	        request.getRequestDispatcher("Connexion.jsp").forward(request, response);
	    }

}
