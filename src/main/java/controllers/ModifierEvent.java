package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import org.owasp.encoder.Encode;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Objects;
import model.Evenement;
import model.Utilisateur;
import dao.EvenementDAO;

@MultipartConfig
/**
 * Servlet implementation class ModifierEvent
 */
public class ModifierEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EvenementDAO evenementDAO;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifierEvent() {
        super();
        // TODO Auto-generated constructor stub
        evenementDAO = new EvenementDAO(); // Initialiser votre DAO
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String idEvenementStr = request.getParameter("idEvenement");
	        String titre = Encode.forHtml(request.getParameter("titre"));
	        String contenu = Encode.forHtml(request.getParameter("contenu"));
	        String auteur = request.getParameter("auteur");
	        int aut = Integer.parseInt(auteur);
	        Part filePart = request.getPart("image"); // Récupère le fichier


	        HttpSession sessi = request.getSession(false);

	        if(sessi != null) {
	        	Utilisateur nome=(Utilisateur) sessi.getAttribute("userName");
	        
	        	if(nome!= null) {
	        		String pro = (String) nome.getprofil();
	        		if(!pro.equals("Lambda")) {
	        		// Vérifier si l'identifiant de l'événement n'est pas null
	    	        if (idEvenementStr != null && !idEvenementStr.isEmpty()) {
	    	            int idEvenement = Integer.parseInt(idEvenementStr);

	    	            // Vérifier si un fichier a été envoyé
	    	            if (filePart != null && filePart.getSize() > 0) {
	    	            	 // Valider le type de fichier
		     	            if (!filePart.getContentType().startsWith("image/")) {
		     	            	response.getWriter().write("[{\"success\": false, \"message\": \"Votre fichier doit être une image.\"}]");
		     					response.setContentType("application/json");
		     		            response.setCharacterEncoding("UTF-8");
		     		            PrintWriter out = response.getWriter();
		     	            
		     	                return;
		     	            }

		     	            // Limiter la taille du fichier (par exemple, 10 Mo)
		     	            if (filePart.getSize() > 10 * 1024 * 1024) {
		     	            	response.getWriter().write("[{\"success\": false, \"message\": \"La taille du fichier ne doit pas dépassée 10 Mo.\"}]");
		     					response.setContentType("application/json");
		     		            response.setCharacterEncoding("UTF-8");
		     		            PrintWriter out = response.getWriter();
		     		            return;
		     	            }

	    	                byte[] photo = null;
	    	                if (filePart != null && filePart.getSize() > 0) {
	    	        			InputStream inputStream = filePart.getInputStream();
	    	        			photo = inputStream.readAllBytes();
	    	         
	    	        			// Stockage du fichier dans le répertoire "images" dans le répertoire "webapp"
	    	        			String nomFichier = filePart.getSubmittedFileName();
	    	        			String cheminStockage = getServletContext().getRealPath("/images/") + nomFichier;
	    	        			try (OutputStream outputStream = new FileOutputStream(cheminStockage)) {
	    	        				outputStream.write(photo);
	    	        			} catch (IOException e) {
	    	        				e.printStackTrace();
	    	        				// Gérer l'exception de stockage
	    	        			} 
	    	        		}
	    	                // Récupérer l'événement à modifier
	    	                Evenement event = evenementDAO.get(idEvenement);

	    	                // Mettre à jour les informations de l'événement
	    	                event.setTitre(titre);
	    	                event.setContenu(contenu);
	    	                event.setAuteur(aut);
	    	                event.setImage(photo);

	    	                // Appeler la méthode de mise à jour dans le DAO
	    	                evenementDAO.update(event);

	    	                // Redirection vers la page JSP appropriée avec une notification et la liste mise à jour des événements
	    	                response.sendRedirect("Success.jsp");
	    	            } else {
	    	                // Si aucun fichier n'a été envoyé, mettre à jour uniquement les informations textuelles de l'événement
	    	                Evenement event = new Evenement();
	    	                event.setIdEvenement(idEvenement);
	    	                event.setTitre(titre);
	    	                event.setContenu(contenu);
	    	                event.setAuteur(aut);

	    	                evenementDAO.update(event);

	    	                // Redirection vers la page JSP appropriée avec une notification et la liste mise à jour des événements
	    	                response.sendRedirect("Success.jsp");
	    	            }
	    	        } else {
	    	            // Si l'identifiant de l'événement est manquant, rediriger vers une page d'erreur
	    	            response.sendRedirect("Success.jsp");
	    	        }
	        	}
	        	else{
	        		response.sendRedirect("Logout");
	        	}
	        }else{
        		response.sendRedirect("Logout");
        	}
	        }
	        
	        
	}

}
