// TODO Auto-generated method stub
		package controllers;

		import com.google.gson.Gson;

		import jakarta.servlet.RequestDispatcher;
		import jakarta.servlet.ServletException;
		import jakarta.servlet.annotation.WebServlet;
		import jakarta.servlet.http.HttpServlet;
		import jakarta.servlet.http.HttpServletRequest;
		import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.DataHandle;
import model.Utilisateur;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
/**
 * Servlet implementation class Treat
 */
public class Treat extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Treat() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Supposons que vous ayez récupéré les données sous forme de Map avec les âges comme clés et le nombre de personnes comme valeurs
				String code_fede =request.getParameter("federation");
				String region=request.getParameter("region");
				String commune = request.getParameter("commune");
				String select = request.getParameter("type_recherchee");
				String departement =request.getParameter("departement");
			
				 HttpSession sessi = request.getSession(false);


			        if(sessi != null) {
			        	Utilisateur nome=(Utilisateur) sessi.getAttribute("userName");
			        
			        	if(nome!= null) {
			        		String pro = (String) nome.getprofil();
			        		if(!pro.equals("Lambda")) {
			        		if(code_fede.isEmpty() && region.isEmpty() && commune.isEmpty() && departement.isEmpty()) {
								response.sendRedirect("AccueilMeta.jsp");
							} else if (select.equals("commune")) {
								 DataHandle dataHandle = new DataHandle();
							     Map<String, Map<String, Integer>> allData = new LinkedHashMap<>();
							    		 allData = dataHandle.getDataByFilter(select, code_fede, commune);
							     // Convertir les données en JSON
							        Gson gson = new Gson();
							        String jsonData = gson.toJson(allData);

							        // Envoyer les données JSON en réponse
							        response.setContentType("application/json");
							        response.setCharacterEncoding("UTF-8");
							        response.getWriter().write(jsonData);
							        request.setAttribute("club", jsonData);
							        RequestDispatcher dispatcher = request.getRequestDispatcher("/AccueilMeta.jsp");
									dispatcher.forward(request, response);
							}
							else if (select.equals("region")) {
								 DataHandle dataHandle = new DataHandle();
							     Map<String, Map<String, Integer>> allData = new LinkedHashMap<>();
							    		 allData=dataHandle.getDataByFilter(select, code_fede,region);
							     // Convertir les données en JSON
							        Gson gson = new Gson();
							        String jsonData = gson.toJson(allData);

							        // Envoyer les données JSON en réponse
							        response.setContentType("application/json");
							        response.setCharacterEncoding("UTF-8");
							        response.getWriter().write(jsonData);
							        request.setAttribute("club", jsonData);
							        RequestDispatcher dispatcher = request.getRequestDispatcher("/AccueilMeta.jsp");
									dispatcher.forward(request, response);
							}
							else if (select.equals("departement")) {
								 DataHandle dataHandle = new DataHandle();
							     Map<String, Map<String, Integer>> allData = new LinkedHashMap<>();
							    		 allData=dataHandle.getDataByFilter(select, code_fede,departement);
							     // Convertir les données en JSON
							        Gson gson = new Gson();
							        String jsonData = gson.toJson(allData);

							        // Envoyer les données JSON en réponse
							        response.setContentType("application/json");
							        response.setCharacterEncoding("UTF-8");
							        response.getWriter().write(jsonData);
							        request.setAttribute("club", jsonData);
							        RequestDispatcher dispatcher = request.getRequestDispatcher("/AccueilMeta.jsp");
									dispatcher.forward(request, response);
							}

			        	}
			        	else{
			        		response.sendRedirect("Logout");
			        	}
			        }	else{
		        		response.sendRedirect("Logout");
		        	}

			        
			        }	        
				       
	}

}
