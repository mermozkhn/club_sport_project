package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

/**
 * Servlet implementation class ManageEven*/

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Evenement;
import model.Utilisateur;
import dao.EvenementDAO;

/**
 * Servlet implementation class ManageEvent
 */
public class ManageEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EvenementDAO evenementDAO;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManageEvent() {
        super();
        evenementDAO = new EvenementDAO(); // Initialize your DAO
        
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// Get the list of events and forward to the JSP
    	
        List<Evenement> eventsList = new ArrayList<>();
        eventsList=evenementDAO.getList();
        request.setAttribute("eventsList", eventsList);
        request.getRequestDispatcher("Success.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String action = request.getParameter("action");
		 HttpSession sessi = request.getSession(false);

	        if(sessi != null) {
	        	Utilisateur nome=(Utilisateur) sessi.getAttribute("userName");
	        
	        	if(nome!= null) {
	        		String pro = (String) nome.getprofil();
	        		if(!pro.equals("Lambda")) {
	        		if (action != null) {
	    	            switch (action) {
	    	                case "modifier":
	    	                    modifierEvenement(request, response);
	    	                    break;
	    	                case "supprimer":
	    	                    supprimerEvenement(request, response);
	    	                    break;
	    	                default:
	    	                    response.sendRedirect("ManageEvent");
	    	                    break;
	    	            }
	    	        } else {
	    	            response.sendRedirect("ManageEvent");
	    	        }
	        	}
	        	else{
	        		response.sendRedirect("Logout");
	        	}
	        	}else{
	        		response.sendRedirect("Logout");
	        	}
	        }
	        
	    }

	    private void modifierEvenement(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        int idEvenement = Integer.parseInt(request.getParameter("idEvenement"));
	        // Redirect to the form with the event id to be modified
	        response.sendRedirect("ModifierEvent.jsp?id="+idEvenement);
	    }

	    private void supprimerEvenement(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        int idEvenement = Integer.parseInt(request.getParameter("idEvenement"));
	        evenementDAO.delete(idEvenement);
	        response.sendRedirect("ManageEvent");
	    }
}
