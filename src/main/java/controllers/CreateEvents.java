package controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.owasp.encoder.Encode;
import dao.EvenementDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import model.Evenement;
import model.Utilisateur;


/**
 * Servlet implementation class CreateEvents
 */
@MultipartConfig
public class CreateEvents extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateEvents() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("Success.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 Part filePart = null;
		 
		 HttpSession sessi = request.getSession(false);

	        if(sessi != null) {
	        	Utilisateur nome=(Utilisateur) sessi.getAttribute("userName");
	        
	        	if(nome!= null) {
	        		String pro = (String) nome.getprofil();
	        		if(!pro.equals("Lambda")) {
		        	
	        		 try {
	     	            // Récupérer les données du formulaire
	     	            String titre = Encode.forHtml(request.getParameter("titre"));
	     	            String contenu = Encode.forHtml(request.getParameter("contenu"));
	     	            filePart = request.getPart("image"); // Récupère l'image
	     	            String auteur = request.getParameter("auteur");
	     	            
	     	            EvenementDAO evenementDAO = new EvenementDAO();

	     	           if(filePart != null && filePart.getSize() > 0) {
	     	        	  // Valider le type de fichier
		     	            if (!filePart.getContentType().startsWith("image/")) {
		     	                request.setAttribute("notification", "Le fichier doit être une image.");
		     	                request.getRequestDispatcher("Success.jsp").forward(request, response);
		     	                return;
		     	            }

		     	            // Limiter la taille du fichier (par exemple, 10 Mo)
		     	            if (filePart.getSize() > 10 * 1024 * 1024) {
		     	            	request.setAttribute("notification", "La taille du fichier ne doit pas dépasser 10 Mo.");
		     	                request.getRequestDispatcher("Success.jsp").forward(request, response);
		     	                return;
		     	            }
	     	           }

	     	            byte[] photo = null;
	     	            if (filePart != null && filePart.getSize() > 0) {
	     	    			InputStream inputStream = filePart.getInputStream();
	     	    			photo = inputStream.readAllBytes();
	     	     
	     	    			// Stockage du fichier dans le répertoire "images" dans le répertoire "webapp"
	     	    			String nomFichier = filePart.getSubmittedFileName();
	     	    			String cheminStockage = getServletContext().getRealPath("/images/") + nomFichier;
	     	    			try (OutputStream outputStream = new FileOutputStream(cheminStockage)) {
	     	    				outputStream.write(photo);
	     	    			} catch (IOException e) {
	     	    				e.printStackTrace();
	     	    				// Gérer l'exception de stockage
	     	    			} 
	     	    		}

	     	            // Créer un objet Evenement avec les données
	     	            Evenement evenement = new Evenement();
	     	            int user = evenementDAO.getId(auteur) ;
	     	            System.out.println(user);
	     	            evenement.setTitre(titre);
	     	            evenement.setContenu(contenu);
	     	            evenement.setImage(photo); // Stocke le chemin relatif de l'image
	     	            evenement.setDateHeure(LocalDateTime.now()); // Définit l'heure actuelle
	     	            evenement.setAuteur(user);//auteur

	     	            // Enregistrer l'événement dans la base de données
	     	            
	     	            evenementDAO.add(evenement);

	     	            
	     	            request.setAttribute("notification", "Votre événement a été créé avec succès !");

	     	            request.getRequestDispatcher("Success.jsp").forward(request, response);
	     	            
	     	        } catch (IOException | ServletException e) {
	     	            e.printStackTrace(); // Affiche l'erreur dans les logs
	     	            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Une erreur s'est produite lors de la création de l'événement");
	     	        } finally {
	     	            // Fermer le flux d'entrée du fichier
	     	            if (filePart != null) {
	     	                filePart.delete(); // Supprimer le fichier temporaire téléchargé
	     	            }
	     	        }
	        	}
	        	else{
	        		response.sendRedirect("Logout");
	        	}
	        	}
	        	else{
	        		response.sendRedirect("Logout");
	        	}
	        }
	        
	       
	}

}
