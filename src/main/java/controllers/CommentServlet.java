package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.owasp.encoder.Encode;
import model.Commentaire;
import model.Utilisateur;

import java.io.IOException;
import java.io.PrintWriter;

import dao.EvenementDAO;

/**
 * Servlet implementation class CommentServlet
 */
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session != null) {
			Utilisateur utilisateur= (Utilisateur) session.getAttribute("userName");
			if(utilisateur == null) {
				response.getWriter().write("[{\"success\": false, \"message\": \"Vous devez être connecté pour commenter.\"}]");
				response.setContentType("application/json");
	            response.setCharacterEncoding("UTF-8");
	            PrintWriter out = response.getWriter();
	            
				//out.flush();   
				return;
			}
			else {
				 String pos = utilisateur.getprofil();
				 if(!pos.isEmpty() && pos.equals("Lambda")) {
					 int IdEvenement = Integer.parseInt(request.getParameter("IdEvenement"));
				        String contenu =Encode.forHtml(request.getParameter("comment"));

				        Commentaire commentaire = new Commentaire();
				        commentaire.setIdUtilisateur(utilisateur.getId_user());
				        commentaire.setIdEvenement(IdEvenement);
				        commentaire.setContenu(contenu);

						EvenementDAO evenementDAO= new EvenementDAO();;
				        boolean success = evenementDAO.addCommentaire(commentaire);

				        response.setContentType("application/json");
				        response.setCharacterEncoding("UTF-8");
				        PrintWriter out = response.getWriter();
				        if (success) {
				        	 int commentCount = evenementDAO.getcommentCount(IdEvenement);
				            response.getWriter().write("[{\"success\": true, \"comment\": " + commentCount + "}]");
				        } else {
				            response.getWriter().write("[{\"success\": false, \"message\": \"Erreur lors de l'ajout du commentaire.\"}]");
				        }
				        //out.flush();
				 }
				 else {
					 response.sendRedirect("Logout");
				 }
			}
		}
      
       
	}

}
