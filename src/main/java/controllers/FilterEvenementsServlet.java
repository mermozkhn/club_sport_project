package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Evenement;
import org.owasp.encoder.Encode;
import java.io.IOException;
import java.util.List;

import dao.EvenementDAO;

/**
 * Servlet implementation class FilterEvenementsServlet
 */
public class FilterEvenementsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FilterEvenementsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String keyword = Encode.forHtml(request.getParameter("keyword"));
	        
	        // Utilisation de la DAO pour récupérer les événements filtrés
	        EvenementDAO evenementDAO = new EvenementDAO();
	        List<Evenement> evenementsFiltres = evenementDAO.getListByKeyword(keyword);
	        
	        request.setAttribute("evenements", evenementsFiltres);
	        request.getRequestDispatcher("sucess.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
