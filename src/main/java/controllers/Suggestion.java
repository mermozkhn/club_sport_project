package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import com.google.gson.Gson;

import dao.ClubDAO;

/**
 * Servlet implementation class Suggestion
 */
public class Suggestion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Suggestion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String query = request.getParameter("suggestion");
		 ClubDAO cu = new ClubDAO();
	        ArrayList<String> suggestions = new ArrayList<>();
	        suggestions=cu.findCo(query);
	        response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        new Gson().toJson(suggestions, response.getWriter());
	        PrintWriter out = response.getWriter();
	        out.print(suggestions.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
