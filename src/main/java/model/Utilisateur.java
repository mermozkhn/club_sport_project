package model;
import org.mindrot.jbcrypt.BCrypt;


public class Utilisateur {
private int Id_user;
private String nom;
private String prenom;
private String E_mail;
private String profil;
private String MDP;


public int getId_user() {
    return Id_user;
}

public void setId_user(int Id_user) {
    this.Id_user = Id_user;
}
  
    public String getnom() {
        return nom;
    }

    public void nom(String nom) {
        this.nom = nom;
    }
    public String getprenom() {
        return prenom;
    }

    public void prenom(String prenom) {
        this.prenom = prenom;
    }
    public String getE_mail() {
        return E_mail;
    }

    public void E_mail(String E_mail) {
        this.E_mail = E_mail;
    }
    public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getProfil() {
		return profil;
	}

	public void setProfil(String profil) {
		this.profil = profil;
	}

	public void setE_mail(String e_mail) {
		E_mail = e_mail;
	}

	public String getprofil() {
        return profil;
    }

    public void profil(String profil) {
        this.profil = profil;
    }
    public String getMDP() {
        return MDP;
    }

    public void setMDP(String MDP) {
        // Hasher le mot de passe avec BCrypt avant de l'affecter
        String hashedPassword = BCrypt.hashpw(MDP, BCrypt.gensalt(12));
        this.MDP = hashedPassword;
    
    }
    public boolean checkPassword(String plainPassword) {
        // Vérifier si le mot de passe correspond au hash stocké
        return BCrypt.checkpw(plainPassword, this.MDP);
    }
    
}

