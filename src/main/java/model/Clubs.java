/**
 * 
 */
package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Mermoz KANHONOU
 *
 */
public class Clubs{
	private int id_club;
	private String Code_commune;
	private String Code_federation;
	private String Nom_federation;
	private String departement;
	private String region;
	private String epa;
	private String club;
	private String total_club;

	
	
	public int getId_club() {
        return id_club;
    }

    public String getCode_commune() {
        return Code_commune;
    }

    public String getCode_federation() {
        return Code_federation;
    }

    public String getNom_federation() {
        return Nom_federation;
    }

    public String getDepartement() {
        return departement;
    }

    public String getRegion() {
        return region;
    }

    public String getTotal_club() {
        return total_club;
    }

    // Setters
    public void setId_club(int id_club) {
        this.id_club = id_club;
    }

    public void setCode_commune(String Code_commune) {
        this.Code_commune = Code_commune;
    }

    public void setCode_federation(String Code_federation) {
        this.Code_federation = Code_federation;
    }

    public void setNom_federation(String Nom_federation) {
        this.Nom_federation = Nom_federation;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setTotal_club(String Total_club) {
        this.total_club = Total_club;
    }

	/**
	 * @return the epa
	 */
	public String getEpa() {
		return epa;
	}

	/**
	 * @param epa the epa to set
	 */
	public void setEpa(String epa) {
		this.epa = epa;
	}

	/**
	 * @return the club
	 */
	public String getClub() {
		return club;
	}

	/**
	 * @param club the club to set
	 */
	public void setClub(String club) {
		this.club = club;
	}


}
