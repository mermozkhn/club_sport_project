package model;

import jakarta.servlet.http.HttpServletRequest;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;

import dao.*;

public class Logger {

	public Logger() {
		// TODO Auto-generated constructor stub
	}

	public static void writelog(HttpServletRequest request, String status) throws UnknownHostException {
        // Récupérer l'adresse IP de l'utilisateur
		InetAddress adress = InetAddress.getLocalHost();
        String ipAddress = adress.getHostAddress();
        
        // Récupérer le navigateur de l'utilisateur
        String userAgent = request.getHeader("User-Agent");
       
        // Récupérer la date et l'heure actuelles
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = now.format(formatter);
        ConnexionDAO co= new ConnexionDAO();
        Boolean isAdd = co.addUser(formattedDateTime, ipAddress, userAgent, status);

        // Écrire le log dans un fichier
        try (FileWriter fw = new FileWriter("connexion.logs.txt", true);
             PrintWriter pw = new PrintWriter(fw)) {
            pw.println("IP: " + ipAddress + " || Navigateur: " + userAgent + " || Date et Heure: " + formattedDateTime + " || Status: " + status);
        } catch (IOException e) {
            System.err.println("Erreur lors de l'écriture dans le fichier de logs : " + e.getMessage());
        }
    }
	private static String getIPv6Address() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    InetAddress inetAddress = inetAddresses.nextElement();
                    if (inetAddress instanceof java.net.Inet6Address && !inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            System.err.println("Erreur lors de la récupération de l'adresse IP : " + e.getMessage());
        }
        return "IPv6 address not found";
    }
}
