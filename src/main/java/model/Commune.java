package model;

public class Commune {

	    private String inseeCode;
	    private String nomCommune;
	    private String codePostal;
	    private String label;
	    private String latitude;
	    private String longitude;
	    private String departmentName;
	    private String departmentNumber;
	    private String regionName;
	    private String regionGeoJsonName;
	    private String 	homm;
	    /**
		 * @return the homm
		 */
		public String getHomm() {
			return homm;
		}

		/**
		 * @param homm the homm to set
		 */
		public void setHomm(String homm) {
			this.homm = homm;
		}

		/**
		 * @return the femm
		 */
		public String getFemm() {
			return femm;
		}

		/**
		 * @param femm the femm to set
		 */
		public void setFemm(String femm) {
			this.femm = femm;
		}

		private String femm;


		public Commune() {
			// TODO Auto-generated constructor stub
		}
		
	    // Getters et setters pour chaque attribut
	    public String getInseeCode() {
	        return inseeCode;
	    }

	    public void setInseeCode(String inseeCode) {
	        this.inseeCode = inseeCode;
	    }

	    public String getNomCommune() {
	        return nomCommune;
	    }

	    public void setNomCommune(String nomCommune) {
	        this.nomCommune = nomCommune;
	    }

	    public String getCodePostal() {
	        return codePostal;
	    }

	    public void setCodePostal(String codePostal) {
	        this.codePostal = codePostal;
	    }

	    public String getLabel() {
	        return label;
	    }

	    public void setLabel(String label) {
	        this.label = label;
	    }

	    public String getLatitude() {
	        return latitude;
	    }

	    public void setLatitude(String latitude) {
	        this.latitude = latitude;
	    }

	    public String getLongitude() {
	        return longitude;
	    }

	    public void setLongitude(String longitude) {
	        this.longitude = longitude;
	    }

	    public String getDepartmentName() {
	        return departmentName;
	    }

	    public void setDepartmentName(String departmentName) {
	        this.departmentName = departmentName;
	    }

	    public String getDepartmentNumber() {
	        return departmentNumber;
	    }

	    public void setDepartmentNumber(String departmentNumber) {
	        this.departmentNumber = departmentNumber;
	    }

	    public String getRegionName() {
	        return regionName;
	    }

	    public void setRegionName(String regionName) {
	        this.regionName = regionName;
	    }

	    public String getRegionGeoJsonName() {
	        return regionGeoJsonName;
	    }

	    public void setRegionGeoJsonName(String regionGeoJsonName) {
	        this.regionGeoJsonName = regionGeoJsonName;
	    }

		@Override
		public String toString() {
			return "Commune [inseeCode=" + inseeCode + ", nomCommune=" + nomCommune + ", codePostal=" + codePostal
					+ ", label=" + label + ", latitude=" + latitude + ", longitude=" + longitude + ", departmentName="
					+ departmentName + ", departmentNumber=" + departmentNumber + ", regionName=" + regionName
					+ ", regionGeoJsonName=" + regionGeoJsonName + "]";
		}

}
