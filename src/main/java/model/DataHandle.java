package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import dao.*;

public class DataHandle {

	private LicenceDAO bdd;
	
	 // Méthode pour récupérer les données en fonction du filtre sélectionné et du genre
    public Map<String, Map<String, Integer>> getDataByFilter(String filter, String code, String critere) {
        // Ici, vous devrez implémenter la logique pour récupérer les données en fonction du filtre sélectionné
        // Par exemple, exécutez une requête SQL pour obtenir les données filtrées
    	bdd = new LicenceDAO();
        // Supposons que vous ayez récupéré les données sous forme de Map avec les âges comme clés et le nombre de personnes comme valeurs
        Map<String, Map<String, Integer>> allData = new HashMap<>();
        
        // Remplissez la map avec les données en fonction du filtre sélectionné
        if (filter.equals("commune")) {
            // Récupérer les données pour la commune sélectionnée
            allData.put("homme", getAgeDataForCommune("homme",code,critere));
            allData.put("femme", getAgeDataForCommune("femme",code,critere));
        } else if (filter.equals("federation")) {
            // Récupérer les données pour la fédération sélectionnée
            allData.put("homme", getAgeDataForFederation("homme",code));
            allData.put("femme", getAgeDataForFederation("femme",code));
        } else if (filter.equals("region")) {
            // Récupérer les données pour la région sélectionnée
            allData.put("homme", getAgeDataForRegion("homme",code,critere));
            allData.put("femme", getAgeDataForRegion("femme",code,critere));
        } else if (filter.equals("departement")) {
            // Récupérer les données pour le département sélectionné
            allData.put("homme", getAgeDataForDepartement("homme",code,critere));
            allData.put("femme", getAgeDataForDepartement("femme",code,critere));
        }
     // Créez une LinkedHashMap pour maintenir l'ordre d'insertion
        Map<String, Map<String, Integer>> sortedAllData = new LinkedHashMap<>();

        // Triez les entrées de allData par clé
        allData.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEachOrdered(entry -> sortedAllData.put(entry.getKey(), entry.getValue()));

        return sortedAllData;
    }

    // Méthode pour récupérer les données d'âge pour les hommes et les femmes pour une commune donnée
    private Map<String, Integer> getAgeDataForCommune(String gender, String code, String critere) {
        // Implémentez la logique pour récupérer les données d'âge pour une commune donnée en fonction du genre
        Map<String, Integer> dataForCommune = new LinkedHashMap<>();
        // Récupérer les données d'âge pour les hommes ou les femmes pour la commune sélectionnée
        Licence lic = new Licence();
        lic=bdd.selFilterCom(critere,code);
        if(gender.equals("homme")) {
        	dataForCommune.put("1_4_ans", lic.getH1_4());
        	dataForCommune.put("5_9_ans", lic.getH5_9());
        	dataForCommune.put("10_14_ans", lic.getH10_14());
        	dataForCommune.put("15_19_ans", lic.getH15_19());
        	dataForCommune.put("20_24_ans", lic.getH20_24());
        	dataForCommune.put("25_29_ans", lic.getH25_29());
        	dataForCommune.put("30_34_ans", lic.getH30_34());
        	dataForCommune.put("35_39_ans", lic.getH35_39());
        	dataForCommune.put("40_44_ans", lic.getH40_44());
        	dataForCommune.put("45_49_ans", lic.getH45_49());
        	dataForCommune.put("50_54_ans", lic.getH50_54());
        	dataForCommune.put("55_59_ans", lic.getH55_59());
        	dataForCommune.put("60_64_ans", lic.getH60_64());
        	dataForCommune.put("65_69_ans", lic.getH65_69());
        	dataForCommune.put("70_74_ans", lic.getH70_74());
        	dataForCommune.put("75_79_ans", lic.getH75_79());
        	dataForCommune.put("80_99_ans", lic.getH80_99());
        	
        }
        else if (gender.equals("femme")) {
        	dataForCommune.put("1_4_ans", lic.getF1_4());
        	dataForCommune.put("5_9_ans", lic.getF5_9());
        	dataForCommune.put("10_14_ans", lic.getF10_14());
        	dataForCommune.put("15_19_ans", lic.getF15_19());
        	dataForCommune.put("20_24_ans", lic.getF20_24());
        	dataForCommune.put("25_29_ans", lic.getF25_29());
        	dataForCommune.put("30_34_ans", lic.getF30_34());
        	dataForCommune.put("35_39_ans", lic.getF35_39());
        	dataForCommune.put("40_44_ans", lic.getF40_44());
        	dataForCommune.put("45_49_ans", lic.getF45_49());
        	dataForCommune.put("50_54_ans", lic.getF50_54());
        	dataForCommune.put("55_59_ans", lic.getF55_59());
        	dataForCommune.put("60_64_ans", lic.getF60_64());
        	dataForCommune.put("65_69_ans", lic.getF65_69());
        	dataForCommune.put("70_74_ans", lic.getF70_74());
        	dataForCommune.put("75_79_ans", lic.getF75_79());
        	dataForCommune.put("80_99_ans", lic.getF80_99());
        }
        return dataForCommune;
    }

    // Méthode pour récupérer les données d'âge pour les hommes et les femmes pour une fédération donnée
    private Map<String, Integer> getAgeDataForFederation(String gender, String code) {
        // Implémentez la logique pour récupérer les données d'âge pour une fédération donnée en fonction du genre
        Map<String, Integer> dataForFederation = new LinkedHashMap<>();
        // Récupérer les données d'âge pour les hommes ou les femmes pour la fédération sélectionnée
        // dataForFederation = ...
        Licence lic = new Licence();
        lic=bdd.selFilterFed(code);
        if(gender.equals("homme")) {
        	dataForFederation.put("1_4_ans", lic.getH1_4());
        	dataForFederation.put("5_9_ans", lic.getH5_9());
        	dataForFederation.put("10_14_ans", lic.getH10_14());
        	dataForFederation.put("15_19_ans", lic.getH15_19());
        	dataForFederation.put("20_24_ans", lic.getH20_24());
        	dataForFederation.put("25_29_ans", lic.getH25_29());
        	dataForFederation.put("30_34_ans", lic.getH30_34());
        	dataForFederation.put("35_39_ans", lic.getH35_39());
        	dataForFederation.put("40_44_ans", lic.getH40_44());
        	dataForFederation.put("45_49_ans", lic.getH45_49());
        	dataForFederation.put("50_54_ans", lic.getH50_54());
        	dataForFederation.put("55_59_ans", lic.getH55_59());
        	dataForFederation.put("60_64_ans", lic.getH60_64());
        	dataForFederation.put("65_69_ans", lic.getH65_69());
        	dataForFederation.put("70_74_ans", lic.getH70_74());
        	dataForFederation.put("75_79_ans", lic.getH75_79());
        	dataForFederation.put("80_99_ans", lic.getH80_99());
        	
        }
        else if (gender.equals("femme")) {
        	dataForFederation.put("1_4_ans", lic.getF1_4());
        	dataForFederation.put("5_9_ans", lic.getF5_9());
        	dataForFederation.put("10_14_ans", lic.getF10_14());
        	dataForFederation.put("15_19_ans", lic.getF15_19());
        	dataForFederation.put("20_24_ans", lic.getF20_24());
        	dataForFederation.put("25_29_ans", lic.getF25_29());
        	dataForFederation.put("30_34_ans", lic.getF30_34());
        	dataForFederation.put("35_39_ans", lic.getF35_39());
        	dataForFederation.put("40_44_ans", lic.getF40_44());
        	dataForFederation.put("45_49_ans", lic.getF45_49());
        	dataForFederation.put("50_54_ans", lic.getF50_54());
        	dataForFederation.put("55_59_ans", lic.getF55_59());
        	dataForFederation.put("60_64_ans", lic.getF60_64());
        	dataForFederation.put("65_69_ans", lic.getF65_69());
        	dataForFederation.put("70_74_ans", lic.getF70_74());
        	dataForFederation.put("75_79_ans", lic.getF75_79());
        	dataForFederation.put("80_99_ans", lic.getF80_99());
        }
        return dataForFederation;
    }

    // Méthode pour récupérer les données d'âge pour les hommes et les femmes pour une région donnée
    private Map<String, Integer> getAgeDataForRegion(String gender, String code, String critere) {
        // Implémentez la logique pour récupérer les données d'âge pour une région donnée en fonction du genre
        Map<String, Integer> dataForRegion = new LinkedHashMap<>();
        // Récupérer les données d'âge pour les hommes ou les femmes pour la région sélectionnée
        // dataForRegion = ...
        Licence lic = new Licence();
        lic=bdd.selFilterReg(critere,code);
        if(gender.equals("homme")) {
        	dataForRegion.put("1_4_ans", lic.getH1_4());
        	dataForRegion.put("5_9_ans", lic.getH5_9());
        	dataForRegion.put("10_14_ans", lic.getH10_14());
        	dataForRegion.put("15_19_ans", lic.getH15_19());
        	dataForRegion.put("20_24_ans", lic.getH20_24());
        	dataForRegion.put("25_29_ans", lic.getH25_29());
        	dataForRegion.put("30_34_ans", lic.getH30_34());
        	dataForRegion.put("35_39_ans", lic.getH35_39());
        	dataForRegion.put("40_44_ans", lic.getH40_44());
        	dataForRegion.put("45_49_ans", lic.getH45_49());
        	dataForRegion.put("50_54_ans", lic.getH50_54());
        	dataForRegion.put("55_59_ans", lic.getH55_59());
        	dataForRegion.put("60_64_ans", lic.getH60_64());
        	dataForRegion.put("65_69_ans", lic.getH65_69());
        	dataForRegion.put("70_74_ans", lic.getH70_74());
        	dataForRegion.put("75_79_ans", lic.getH75_79());
        	dataForRegion.put("80_99_ans", lic.getH80_99());
        	
        }
        else if (gender.equals("femme")) {
        	dataForRegion.put("1_4_ans", lic.getF1_4());
        	dataForRegion.put("5_9_ans", lic.getF5_9());
        	dataForRegion.put("10_14_ans", lic.getF10_14());
        	dataForRegion.put("15_19_ans", lic.getF15_19());
        	dataForRegion.put("20_24_ans", lic.getF20_24());
        	dataForRegion.put("25_29_ans", lic.getF25_29());
        	dataForRegion.put("30_34_ans", lic.getF30_34());
        	dataForRegion.put("35_39_ans", lic.getF35_39());
        	dataForRegion.put("40_44_ans", lic.getF40_44());
        	dataForRegion.put("45_49_ans", lic.getF45_49());
        	dataForRegion.put("50_54_ans", lic.getF50_54());
        	dataForRegion.put("55_59_ans", lic.getF55_59());
        	dataForRegion.put("60_64_ans", lic.getF60_64());
        	dataForRegion.put("65_69_ans", lic.getF65_69());
        	dataForRegion.put("70_74_ans", lic.getF70_74());
        	dataForRegion.put("75_79_ans", lic.getF75_79());
        	dataForRegion.put("80_99_ans", lic.getF80_99());
        }
        return dataForRegion;
    }

    // Méthode pour récupérer les données d'âge pour les hommes et les femmes pour un département donné
    private Map<String, Integer> getAgeDataForDepartement(String gender, String code, String critere) {
        // Implémentez la logique pour récupérer les données d'âge pour un département donné en fonction du genre
        Map<String, Integer> dataForDepartement = new LinkedHashMap<>();
        // Récupérer les données d'âge pour les hommes ou les femmes pour le département sélectionné
        // dataForDepartement = ...
        Licence lic = new Licence();
        lic=bdd.selFilterDep(critere,code);
        if(gender.equals("homme")) {
        	dataForDepartement.put("1_4_ans", lic.getH1_4());
        	dataForDepartement.put("5_9_ans", lic.getH5_9());
        	dataForDepartement.put("10_14_ans", lic.getH10_14());
        	dataForDepartement.put("15_19_ans", lic.getH15_19());
        	dataForDepartement.put("20_24_ans", lic.getH20_24());
        	dataForDepartement.put("25_29_ans", lic.getH25_29());
        	dataForDepartement.put("30_34_ans", lic.getH30_34());
        	dataForDepartement.put("35_39_ans", lic.getH35_39());
        	dataForDepartement.put("40_44_ans", lic.getH40_44());
        	dataForDepartement.put("45_49_ans", lic.getH45_49());
        	dataForDepartement.put("50_54_ans", lic.getH50_54());
        	dataForDepartement.put("55_59_ans", lic.getH55_59());
        	dataForDepartement.put("60_64_ans", lic.getH60_64());
        	dataForDepartement.put("65_69_ans", lic.getH65_69());
        	dataForDepartement.put("70_74_ans", lic.getH70_74());
        	dataForDepartement.put("75_79_ans", lic.getH75_79());
        	dataForDepartement.put("80_99_ans", lic.getH80_99());
        	
        }
        else if (gender.equals("femme")) {
        	dataForDepartement.put("1_4_ans", lic.getF1_4());
        	dataForDepartement.put("5_9_ans", lic.getF5_9());
        	dataForDepartement.put("10_14_ans", lic.getF10_14());
        	dataForDepartement.put("15_19_ans", lic.getF15_19());
        	dataForDepartement.put("20_24_ans", lic.getF20_24());
        	dataForDepartement.put("25_29_ans", lic.getF25_29());
        	dataForDepartement.put("30_34_ans", lic.getF30_34());
        	dataForDepartement.put("35_39_ans", lic.getF35_39());
        	dataForDepartement.put("40_44_ans", lic.getF40_44());
        	dataForDepartement.put("45_49_ans", lic.getF45_49());
        	dataForDepartement.put("50_54_ans", lic.getF50_54());
        	dataForDepartement.put("55_59_ans", lic.getF55_59());
        	dataForDepartement.put("60_64_ans", lic.getF60_64());
        	dataForDepartement.put("65_69_ans", lic.getF65_69());
        	dataForDepartement.put("70_74_ans", lic.getF70_74());
        	dataForDepartement.put("75_79_ans", lic.getF75_79());
        	dataForDepartement.put("80_99_ans", lic.getF80_99());
        }
        return dataForDepartement;
    }

}
