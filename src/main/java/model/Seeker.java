package model;

import jakarta.servlet.http.HttpServletRequest;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;

import dao.*;

public class Seeker {

	public Seeker() {
		// TODO Auto-generated constructor stub
	}
	
	public static void writelog(HttpServletRequest request, String critere, String value, String fede) throws UnknownHostException {
        // Récupérer l'adresse IP de l'utilisateur
		InetAddress adress = InetAddress.getLocalHost();
        String ipAddress = adress.getHostAddress();
        
        RechercheDAO co= new RechercheDAO();
        Boolean isAdd = co.addUser(critere, value, fede, ipAddress);

        // Écrire le log dans un fichier
        try (FileWriter fw = new FileWriter("recherche.logs.txt", true);
             PrintWriter pw = new PrintWriter(fw)) {
            pw.println("IP: " + ipAddress + " || Recherche par: " + critere + " || suivant : " + value + " || FEDE : " + fede);
         
        } catch (IOException e) {
            System.err.println("Erreur lors de l'écriture dans le fichier de logs : " + e.getMessage());
        }
        
        
    }
	private static String getIPv6Address() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    InetAddress inetAddress = inetAddresses.nextElement();
                    if (inetAddress instanceof java.net.Inet6Address && !inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            System.err.println("Erreur lors de la récupération de l'adresse IP : " + e.getMessage());
        }
        return "IPv6 address not found";
    }

}
