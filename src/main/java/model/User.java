/**
 * 
 */
package model;

/**
 * @author Mermoz KANHONOU
 *
 */
public class User {

	private int Id_user;
	private String nom;
	private String prenom;
	private String E_mail;
	private String profil;
	private String MDP;

	
	public int getId_user() {
	    return Id_user;
	}

	public void setId_user(int Id_user) {
	    this.Id_user = Id_user;
	}
	  
	    public String getnom() {
	        return nom;
	    }

	    public void nom(String nom) {
	        this.nom = nom;
	    }
	    public String getprenom() {
	        return prenom;
	    }

	    public void prenom(String prenom) {
	        this.prenom = prenom;
	    }
	    public String getE_mail() {
	        return E_mail;
	    }

	    public void E_mail(String E_mail) {
	        this.E_mail = E_mail;
	    }
	    public String getprofil() {
	        return profil;
	    }

	    public void profil(String profil) {
	        this.profil = profil;
	    }
	    public String getMDP() {
	        return MDP;
	    }

	    public void MDP(String MDP) {
	        this.MDP = MDP;
	    }

}
