package model;

public class Federation {
	    private String fede;
		private String code;
		/**
		 * @return the code
		 */
		public String getCode() {
			return code;
		}

		/**
		 * @param code the code to set
		 */
		public void setCode(String code) {
			this.code = code;
		}

		/**
		 * @return the fede
		 */
		public String getFede() {
			return fede;
		}

		/**
		 * @param fede the fede to set
		 */
		public void setFede(String fede) {
			this.fede = fede;
		}

}
