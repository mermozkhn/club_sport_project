package model;

import java.util.ArrayList;

public class Licence {
	    private int Id_license;
	    private String Code_Commune;
        private String Nom_Commune;
        private String Region;
        private String departement;
        private String code;
        private String federation;
		private int H1_4;
	    private int H5_9;
	    private int H10_14;
	    private int H15_19;
	    private int H20_24;
	    private int H25_29;
	    private int H30_34;
	    private int H35_39;
	    private int H40_44;
	    private int H45_49;
	    private int H50_54;
	    private int H55_59;
	    
		private int H60_64;
	    private int H65_69;
	    private int H70_74;
	    private int H75_79;
	    private int H80_99;
	    private int F1_4;
	    private int F5_9;
	    private int F10_14;
	    private int F15_19;
	    private int F20_24;
	    private int F25_29;
	    private int F30_34;
	    private int F35_39;
	    private int F40_44;
	    private int F45_49;
	    private int F50_54;
	    private int F55_59;
		private int F60_64;
	    private int F65_69;
	    private int F70_74;
	    private int F75_79;
	    private int F80_99;
		private int total;
	    
	   

	    	 public int getId_license() {
	    	        return Id_license;
	    	    }

	    	    public void setId_license(int Id_license) {
	    	        this.Id_license = Id_license;
	    	    }
	    	 public String getCode_Commune() {
	    	        return Code_Commune;
	    	    }

	    	    public void setCode_Commune(String Code_Commune) {
	    	        this.Code_Commune = Code_Commune;
	    	    }

	    	   
	    	    public String getNom_Commune() {
	    	        return Nom_Commune;
	    	    }

	    	    public void setNom_Commune(String Nom_Commune) {
	    	        this.Nom_Commune = Nom_Commune;
	    	    }

	    	    /**
	    		 * @return the departement
	    		 */
	    		public String getDepartement() {
	    			return departement;
	    		}

	    		/**
	    		 * @param departement the departement to set
	    		 */
	    		public String getFederation() {
	    			return federation;
	    		}

	    		public void setFederation(String federation) {
	    			this.federation = federation;
	    		}
	    		public String getCode() {
	    			return code;
	    		}

	    		public void setCode(String code) {
	    			this.code = code;
	    		}
	    		public void setDepartement(String departement) {
	    			this.departement = departement;
	    		}

	    		
	    	    public String getRegion() {
	    	        return Region;
	    	    }

	    	    public void setRegion(String Région) {
	    	        this.Region = Région;
	    	    }
	    	    public int getH1_4() {
	    			return H1_4;
	    		}

	    		public void setH1_4(int h1_4) {
	    			H1_4 = h1_4;
	    		}

	    		public int getH5_9() {
	    			return H5_9;
	    		}

	    		public void setH5_9(int h5_9) {
	    			H5_9 = h5_9;
	    		}

	    		public int getH10_14() {
	    			return H10_14;
	    		}

	    		public void setH10_14(int h10_14) {
	    			H10_14 = h10_14;
	    		}

	    		public int getH15_19() {
	    			return H15_19;
	    		}

	    		public void setH15_19(int h15_19) {
	    			H15_19 = h15_19;
	    		}

	    		public int getH20_24() {
	    			return H20_24;
	    		}

	    		public void setH20_24(int h20_24) {
	    			H20_24 = h20_24;
	    		}

	    		public int getH25_29() {
	    			return H25_29;
	    		}

	    		public void setH25_29(int h25_29) {
	    			H25_29 = h25_29;
	    		}

	    		public int getH30_34() {
	    			return H30_34;
	    		}

	    		public void setH30_34(int h30_34) {
	    			H30_34 = h30_34;
	    		}

	    		public int getH35_39() {
	    			return H35_39;
	    		}

	    		public void setH35_39(int h35_39) {
	    			H35_39 = h35_39;
	    		}

	    		public int getH40_44() {
	    			return H40_44;
	    		}

	    		public void setH40_44(int h40_44) {
	    			H40_44 = h40_44;
	    		}

	    		public int getH45_49() {
	    			return H45_49;
	    		}

	    		public void setH45_49(int h45_49) {
	    			H45_49 = h45_49;
	    		}

	    		public int getH50_54() {
	    			return H50_54;
	    		}

	    		public void setH50_54(int h50_54) {
	    			H50_54 = h50_54;
	    		}

	    		public int getH55_59() {
	    			return H55_59;
	    		}

	    		public void setH55_59(int h55_59) {
	    			H55_59 = h55_59;
	    		}


	    		public int getH60_64() {
	    			return H60_64;
	    		}

	    		public void setH60_64(int h60_64) {
	    			H60_64 = h60_64;
	    		}

	    		public int getH65_69() {
	    			return H65_69;
	    		}

	    		public void setH65_69(int h65_69) {
	    			H65_69 = h65_69;
	    		}

	    		public int getH70_74() {
	    			return H70_74;
	    		}

	    		public void setH70_74(int h70_74) {
	    			H70_74 = h70_74;
	    		}

	    		public int getH75_79() {
	    			return H75_79;
	    		}

	    		public void setH75_79(int h75_79) {
	    			H75_79 = h75_79;
	    		}

	    		public int getH80_99() {
	    			return H80_99;
	    		}

	    		public void setH80_99(int h80_99) {
	    			H80_99 = h80_99;
	    		}

	    		public int getF1_4() {
	    			return F1_4;
	    		}

	    		public void setF1_4(int f1_4) {
	    			F1_4 = f1_4;
	    		}

	    		public int getF5_9() {
	    			return F5_9;
	    		}

	    		public void setF5_9(int f5_9) {
	    			F5_9 = f5_9;
	    		}

	    		public int getF10_14() {
	    			return F10_14;
	    		}

	    		public void setF10_14(int f10_14) {
	    			F10_14 = f10_14;
	    		}

	    		public int getF15_19() {
	    			return F15_19;
	    		}

	    		public void setF15_19(int f15_19) {
	    			F15_19 = f15_19;
	    		}

	    		public int getF20_24() {
	    			return F20_24;
	    		}

	    		public void setF20_24(int f19_24) {
	    			F20_24 = f19_24;
	    		}

	    		public int getF25_29() {
	    			return F25_29;
	    		}

	    		public void setF25_29(int f25_29) {
	    			F25_29 = f25_29;
	    		}
	    		public int getF45_49() {
	    			return F45_49;
	    		}

	    		public void setF45_49(int f45_49) {
	    			F45_49 = f45_49;
	    		}

	    		public int getF50_54() {
	    			return F50_54;
	    		}

	    		public void setF50_54(int f50_54) {
	    			F50_54 = f50_54;
	    		}

	    		public int getF55_59() {
	    			return F55_59;
	    		}

	    		public void setF55_59(int f55_59) {
	    			F55_59 = f55_59;
	    		}


	    		public int getF30_34() {
	    			return F30_34;
	    		}

	    		public void setF30_34(int f30_34) {
	    			F30_34 = f30_34;
	    		}

	    		public int getF35_39() {
	    			return F35_39;
	    		}

	    		public void setF35_39(int f35_39) {
	    			F35_39 = f35_39;
	    		}

	    		public int getF40_44() {
	    			return F40_44;
	    		}

	    		public void setF40_44(int f40_44) {
	    			F40_44 = f40_44;
	    		}

	    		public int getF60_64() {
	    			return F60_64;
	    		}

	    		public void setF60_64(int f60_64) {
	    			F60_64 = f60_64;
	    		}

	    		public int getF65_69() {
	    			return F65_69;
	    		}

	    		public void setF65_69(int f65_69) {
	    			F65_69 = f65_69;
	    		}

	    		public int getF70_74() {
	    			return F70_74;
	    		}

	    		public void setF70_74(int f70_74) {
	    			F70_74 = f70_74;
	    		}

	    		public int getF75_79() {
	    			return F75_79;
	    		}

	    		public void setF75_79(int f75_79) {
	    			F75_79 = f75_79;
	    		}

	    		public int getF80_99() {
	    			return F80_99;
	    		}

	    		public void setF80_99(int f80_99) {
	    			F80_99 = f80_99;
	    		}

	    		public int getTotal() {
	    			return total;
	    		}

	    		public void setTotal(int total) {
	    			this.total = total;
	    		}


  }
