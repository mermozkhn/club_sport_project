package model;

public class CalculeDistance {

	public CalculeDistance() {
		// TODO Auto-generated constructor stub
	}
	// Méthode pour calculer la distance entre deux points géographiques en utilisant la formule haversine
		public static  double calculateDistance(double latuser, double longuser, double lat2, double lon2) {
			// Rayon de la Terre en kilomètres
			final double R = 6371.0;
	 
			// Convertir les latitudes et longitudes en radians
			double lat1Rad = Math.toRadians(latuser);
			double lon1Rad = Math.toRadians(longuser);
			double lat2Rad = Math.toRadians(lat2);
			double lon2Rad = Math.toRadians(lon2);
			
			// Calculer la différence de latitude et de longitude
			double dLat = lat2Rad - lat1Rad;
			double dLon = lon2Rad - lon1Rad;
	 
			// Calculer la distance en utilisant la formule haversine
			double a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(lat1Rad) * Math.cos(lat2Rad) * Math.pow(Math.sin(dLon / 2), 2);
			double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			double distance = R * c;
			distance = Math.round(distance * 100.0) / 100.0;
			return distance;
		}

}
