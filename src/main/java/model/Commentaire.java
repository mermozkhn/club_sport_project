package model;

import java.time.LocalDateTime;

public class Commentaire {
    private int idCommentaire;
    private int idEvenement;
    private int idUtilisateur;
    private String contenu;
    private LocalDateTime dateHeureCommentaire;

    // Getters et setters
    public int getIdCommentaire() {
        return idCommentaire;
    }

    public void setIdCommentaire(int idCommentaire) {
        this.idCommentaire = idCommentaire;
    }

    public int getIdEvenement() {
        return idEvenement;
    }

    public void setIdEvenement(int idEvenement) {
        this.idEvenement = idEvenement;
    }
    
    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public LocalDateTime getDateHeureCommentaire() {
        return dateHeureCommentaire;
    }

    public void setDateHeureCommentaire(LocalDateTime dateHeureCommentaire) {
        this.dateHeureCommentaire = dateHeureCommentaire;
    }

	public int getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
}