package model;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Base64;

public class Evenement {
    private int idEvenement;  // Assurez-vous que cet attribut existe
    private String titre;
    private String contenu;
    private LocalDateTime dateHeure;
    private int auteur;
    private byte[] image;

    // Getters et setters
    		public String getBase64Image() {
        if (image != null && image.length > 0) {
            return Base64.getEncoder().encodeToString(image);
        } else {
            return null;
        }
    }

    @Override
			public String toString() {
				return "Evenement [idEvenement=" + idEvenement + ", titre=" + titre + ", contenu=" + contenu
						+ ", image=" + image + ", dateHeure=" + dateHeure + ", auteur=" + auteur  + "]";
			}

	public int getIdEvenement() {
        return idEvenement;
    }

    public void setIdEvenement(int idEvenement) {
        this.idEvenement = idEvenement;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public LocalDateTime getDateHeure() {
        return dateHeure;
    }

    public void setDateHeure(LocalDateTime dateHeure) {
        this.dateHeure = dateHeure;
    }

    public int getAuteur() {
        return auteur;
    }

    public void setAuteur(int auteur) {
        this.auteur = auteur;
    }
}
