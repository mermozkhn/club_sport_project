package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;



public class DBDAO{

	
	final String URL   = "jdbc:mysql://localhost:3306/club_sport";
	final String LOGIN = "root";
	final String PASS  = "root";
	Boolean connected = false;
	Connection conn= null;
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			}
	}
	

	protected void dbConnect() {
		try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(URL, LOGIN, PASS); // Établissement de la connexion à la base de données
            System.out.println("Connexion avec succès.");
            connected = true;
        } catch (ClassNotFoundException e) {
            System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
        } catch (SQLException e) {
            System.err.println("Erreur lors de la connexion à la base de données : " + e.getMessage());
   }
	}
	protected void dbClose() {
		 if (conn != null) {
	            try {
	            	connected = false;
	                conn.close(); // Fermeture de la connexion à la base de données
	                System.out.println("Connexion fermée avec succès.");
	            } catch (SQLException e) {
	            	e.printStackTrace();
	                System.err.println("Erreur lors de la fermeture de la connexion à la base de données : " + e.getMessage());
	            }
	           
	        }
	}
	
	
	
}
