package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.mindrot.jbcrypt.BCrypt;

import model.Utilisateur;

public class UtilisateurDAO extends DBDAO {

	
		
		public int add(Utilisateur utilisateur) {
			Connection con = null;
			PreparedStatement ps = null;
			int returnValue = 0;

			// connexion a la base de donnees
			try {

				// tentative de connexion
				con = DriverManager.getConnection(URL, LOGIN, PASS);
				// preparation de l'instruction SQL, chaque ? represente une valeur
				// a communiquer dans l'insertion.
				// les getters permettent de recuperer les valeurs des attributs souhaites
				ps = con.prepareStatement("INSERT INTO users(idusers,nom,prenom,motdepass,email,profil) VALUES(?, ?, ?, ?, ?, ?)");
				ps.setInt(1, utilisateur.getId_user());
				ps.setString(2, utilisateur.getnom());
				ps.setString(3, utilisateur.getprenom());
				ps.setString(4, utilisateur.getE_mail());
				ps.setString(5, utilisateur.getMDP());
				ps.setString(6, utilisateur.getprofil());
				
				

				// Execution de la requete
				returnValue = ps.executeUpdate();

			} catch (Exception e) {
				if (e.getMessage().contains("ORA-00001"))
					System.out.println("Cet identifiant de fournisseur existe déjà. Ajout impossible !");
				else
					e.printStackTrace();
			} finally {
				// fermeture du preparedStatement et de la connexion
				try {
					if (ps != null) {
						ps.close();
					}
				} catch (Exception ignore) {
				}
				try {
					if (con != null) {
						con.close();
					}
				} catch (Exception ignore) {
				}
			}
			return returnValue;
		}

		

		/**
		 * Permet de recuperer un fournisseur a partir de sa reference
		 * 
		 * @param reference la reference du fournisseur a recuperer
		 * @return le fournisseur trouve;
		 * 			null si aucun fournisseur ne correspond a cette reference
		 */
		public Utilisateur get(int Id_user) {
			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			 Utilisateur use = new Utilisateur();

			// connexion a la base de donnees
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT * FROM UtilisateurWHERE id = ?");
				ps.setInt(1, Id_user);

				// on execute la requete
				// rs contient un pointeur situe juste avant la premiere ligne retournee
				rs = ps.executeQuery();
				// passe a la premiere (et unique) ligne retournee
				if (rs.next()) {
					use.setId_user(rs.getInt("idusers"));
            		use.setE_mail(rs.getString("email"));
            		use.setNom(rs.getString("nom"));
            		use.setPrenom(rs.getString("prenom"));
            		use.setProfil(rs.getString("profil"));
					return use;
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				// fermeture du ResultSet, du PreparedStatement et de la Connexion
				try {
					if (rs != null) {
						rs.close();
					}
				} catch (Exception ignore) {
				}
				try {
					if (ps != null) {
						ps.close();
					}
				} catch (Exception ignore) {
				}
				try {
					if (con != null) {
						con.close();
					}
				} catch (Exception ignore) {
				}
			}
			return use;
		}

		public Utilisateur userConnection(String E_mail, String enteredPassword) {
		    Connection con = null;
		    PreparedStatement ps = null;
		    ResultSet rs = null;
		    Utilisateur returnValue = null;

		    // connexion à la base de données
		    try {
		        con = DriverManager.getConnection(URL, LOGIN, PASS);
		        ps = con.prepareStatement("SELECT * FROM users WHERE email = ?");
		        ps.setString(1, E_mail);
		        Utilisateur use = new Utilisateur();

		        // Exécution de la requête
		        rs = ps.executeQuery();

		        // Si un utilisateur correspondant à l'e-mail est trouvé
		        if (rs.next()) {
		            // Récupération du mot de passe haché stocké dans la base de données
		            String hashedPasswordFromDB = rs.getString("motdepass");

		            // Vérification si le mot de passe entré correspond au mot de passe hashé dans la base de données
		            System.out.println("Mot de passe entré : " + enteredPassword);
		            System.out.println("Mot de passe haché depuis la base de données : " + hashedPasswordFromDB);
		            if (checkPassword(enteredPassword, hashedPasswordFromDB)) {
		                // Authentification réussie, création de l'objet Utilisateur
		                System.out.println("Mot de passe correct !");
		                use.setId_user(rs.getInt("idusers"));
	            		use.setE_mail(rs.getString("email"));
	            		use.setNom(rs.getString("nom"));
	            		use.setPrenom(rs.getString("prenom"));
	            		use.setProfil(rs.getString("profil"));
						return use;
		              
		            } else {
		                System.out.println("Mot de passe incorrect !");
		            }
		        }
		    } catch (SQLException ee) {
		        ee.printStackTrace();
		    } finally {
		        // fermeture du ResultSet, du PreparedStatement et de la Connexion
		        try {
		            if (rs != null) {
		                rs.close();
		            }
		        } catch (SQLException ignore) {
		        }
		        try {
		            if (ps != null) {
		                ps.close();
		            }
		        } catch (SQLException ignore) {
		        }
		        try {
		            if (con != null) {
		                con.close();
		            }
		        } catch (SQLException ignore) {
		        }
		    }
		    return returnValue;
		}
		private boolean checkPassword(String enteredPassword, String hashedPasswordFromDB) {
		    return BCrypt.checkpw(enteredPassword, hashedPasswordFromDB);
		}

		
		public String getProfil(String E_mail, String enteredPassword) {
		    Connection con = null;
		    PreparedStatement ps = null;
		    ResultSet rs = null;
		    String returnValue = null;

		    // connexion à la base de données
		    try {
		        con = DriverManager.getConnection(URL, LOGIN, PASS);
		        ps = con.prepareStatement("SELECT motdepass, profil FROM users WHERE email = ?");

		        ps.setString(1, E_mail);

		        // exécution de la requête
		        rs = ps.executeQuery();
		        // si l'utilisateur est trouvé dans la base de données
		        if (rs.next()) {
		            String hashedPasswordFromDB = rs.getString("motdepass");
		            // vérification du mot de passe hashé
		            if (BCrypt.checkpw(enteredPassword, hashedPasswordFromDB)) {
		                // si les mots de passe correspondent, récupérer le profil de l'utilisateur
		                returnValue = rs.getString("profil");
		            }
		        }
		    } catch (Exception ee) {
		        ee.printStackTrace();
		    } finally {
		        // fermeture du ResultSet, du PreparedStatement et de la Connexion
		        try {
		            if (rs != null) {
		                rs.close();
		            }
		        } catch (Exception ignore) {
		        }
		        try {
		            if (ps != null) {
		                ps.close();
		            }
		        } catch (Exception ignore) {
		        }
		        try {
		            if (con != null) {
		                con.close();
		            }
		        } catch (Exception ignore) {
		        }
		    }
		    return returnValue;
		}
	
}
