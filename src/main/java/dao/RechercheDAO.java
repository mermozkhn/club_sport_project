package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RechercheDAO extends DBDAO{

	public RechercheDAO() {
		// TODO Auto-generated constructor stub
	}
	
	
	public boolean addUser(String nom, String prenom, String email, String motdepass) {
        PreparedStatement stmt = null;
        try {
            dbConnect();
            if (!connected) {
                System.err.println("La connexion à la base de données n'a pas pu être établie.");
                return false;
            }

            String query = "INSERT INTO recherche (critere, value, federation, ipadress) VALUES (?, ?, ?, ?)";
            stmt = conn.prepareStatement(query);
            stmt.setString(1, nom);
            stmt.setString(2, prenom);
            stmt.setString(3, email);
            stmt.setString(4, motdepass);
           
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            System.err.println("Erreur lors de l'ajout de l'utilisateur : " + e.getMessage());
            e.printStackTrace();
            return false;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    System.err.println("Erreur lors de la fermeture de la session : " + e.getMessage());
                }
            }
            dbClose();
        }
    }

}
