package dao;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import model.Commentaire;
import model.Evenement;

public class EvenementDAO {

    private ConnexionDAO connexionDAO;

    public EvenementDAO() {
        connexionDAO = new ConnexionDAO();
    }

    public int add(Evenement evenement) {
        Connection con = null;
        PreparedStatement ps = null;
        int returnValue = 0;

        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;  // Utilisation de la connexion de ConnexionDAO
            ps = con.prepareStatement("INSERT INTO evenement (titre, contenu, image, dateHeure, auteur) VALUES (?, ?, ?, ?, ?)");
            ps.setString(1, evenement.getTitre());
            ps.setString(2, evenement.getContenu());
            ps.setBytes(3, evenement.getImage());
            ps.setTimestamp(4, Timestamp.valueOf(evenement.getDateHeure()));
            ps.setInt(5, evenement.getAuteur());
            returnValue = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connexionDAO.dbClose();
        }
        return returnValue;
    }

    public int update(Evenement evenement) {
        Connection con = null;
        PreparedStatement ps = null;
        int returnValue = 0;

        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
            ps = con.prepareStatement("UPDATE evenement SET titre = ?, contenu = ?, image = ?, dateHeure = ?, auteur = ? WHERE idEvenement = ?");
            ps.setString(1, evenement.getTitre());
            ps.setString(2, evenement.getContenu());
            ps.setBytes(3, evenement.getImage());
           /* ps.setTimestamp(4, Timestamp.valueOf(evenement.getDateHeure()));*/
            LocalDateTime dateTime = evenement.getDateHeure();
            if (dateTime == null) {
                dateTime = LocalDateTime.now(); // Use the current date and time if null
            }
            ps.setTimestamp(4, Timestamp.valueOf(dateTime));
            ps.setInt(5, evenement.getAuteur());
            ps.setInt(6, evenement.getIdEvenement());
            returnValue = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connexionDAO.dbClose();
        }
        return returnValue;
    }

    public int delete(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        int returnValue = 0;

        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
            ps = con.prepareStatement("DELETE FROM evenement WHERE idEvenement = ?");
            ps.setInt(1, id);
            returnValue = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connexionDAO.dbClose();
        }
        return returnValue;
    }
    public List<Evenement> getListByKeyword(String keyword) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Evenement> evenements = new ArrayList<>();

        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
            
            // Requête SQL pour récupérer les événements contenant le mot-clé dans leur titre ou contenu
            String query = "SELECT * FROM evenement WHERE titre LIKE ? OR contenu LIKE ?";
            ps = con.prepareStatement(query);
            ps.setString(1, "%" + keyword + "%");
            ps.setString(2, "%" + keyword + "%");
            rs = ps.executeQuery();

            while (rs.next()) {
                Evenement evenement = new Evenement();
                evenement.setIdEvenement(rs.getInt("idEvenement"));
                evenement.setTitre(rs.getString("titre"));
                evenement.setContenu(rs.getString("contenu"));
                evenement.setImage(rs.getBytes("image"));
                evenement.setDateHeure(rs.getTimestamp("dateHeure").toLocalDateTime());
                evenement.setAuteur(rs.getInt("auteur"));
                evenements.add(evenement);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connexionDAO.dbClose();
        }
        return evenements;
    }

 // Méthode pour ajouter un like à un événement
    public boolean likeEvent(int IdUtilisateur, int IdEvenement) {
    	 if (hasLikedEvent(IdUtilisateur, IdEvenement)) {
    	        return false; // User has already liked this event
    	    }
    	Connection con = null;
        PreparedStatement ps = null;
        boolean success = false;

        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
            ps = con.prepareStatement("INSERT INTO likes (IdUtilisateur, IdEvenement) VALUES (?, ?)");
            ps.setInt(1, IdUtilisateur);
            ps.setInt(2, IdEvenement);
            int rowsAffected = ps.executeUpdate();
            success = (rowsAffected > 0);
          
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connexionDAO.dbClose();
        }
        return success;
    }
    //Methode qui supprime le like 
    public boolean likeDelete(int IdUtilisateur, int IdEvenement) {
   	Connection con = null;
       PreparedStatement ps = null;
       boolean success = false;

       try {
           connexionDAO.dbConnect();
           con = connexionDAO.conn;
           ps = con.prepareStatement("Delete FROM likes where idUtilisateur=? AND idEvenement=? ");
           ps.setInt(1, IdUtilisateur);
           ps.setInt(2, IdEvenement);
           int rowsAffected = ps.executeUpdate();
           success = (rowsAffected > 0);
         
       } catch (SQLException e) {
           e.printStackTrace();
       } finally {
           connexionDAO.dbClose();
       }
       return success;
   }
    
 // Méthode pour obtenir le nombre de likes pour un événement
    public int getLikeCount(int IdEvenement) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int likeCount = 0;

        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
            ps = con.prepareStatement("SELECT COUNT(*) FROM likes WHERE IdEvenement = ?");
            ps.setInt(1, IdEvenement);
            rs = ps.executeQuery();
            if (rs.next()) {
                likeCount = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            connexionDAO.dbClose();
        }
        return likeCount;
    }
    
    public String getNom(int IdEvenement) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String likeCount ="";

        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
            ps = con.prepareStatement("SELECT nom FROM users WHERE idusers = ?");
            ps.setInt(1, IdEvenement);
            rs = ps.executeQuery();
            if (rs.next()) {
                likeCount = rs.getString("nom");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            connexionDAO.dbClose();
        }
        return likeCount;
    }
    public boolean hasLikedEvent(int idUtilisateur, int idEvenement) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean hasLiked = false;

        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
            ps = con.prepareStatement("SELECT COUNT(*) FROM likes WHERE idUtilisateur = ? AND idEvenement = ?");
            ps.setInt(1, idUtilisateur);
            ps.setInt(2, idEvenement);
            rs = ps.executeQuery();
            if (rs.next()) {
                hasLiked = rs.getInt(1) > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connexionDAO.dbClose();
        }
        return hasLiked;
    }
 // Méthode pour obtenir le nombre de commentaires pour un événement
    public int getcommentCount(int IdEvenement) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int commentCount = 0;

        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
            ps = con.prepareStatement("SELECT COUNT(*) FROM commentaire WHERE idEvenement = ?");
            ps.setInt(1, IdEvenement);
            rs = ps.executeQuery();
            if (rs.next()) {
                commentCount = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            connexionDAO.dbClose();
        }
        return commentCount;
    }


        // Méthode pour obtenir les commentaires
        public List<Commentaire> getCommentaires(int idEvenement) {
            Connection con = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            List<Commentaire> commentaires = new ArrayList<>();

            try {
                connexionDAO.dbConnect();
                con = connexionDAO.conn;
                ps = con.prepareStatement("SELECT * FROM commentaire WHERE idEvenement = ?");
                ps.setInt(1, idEvenement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    Commentaire commentaire = new Commentaire();
                    commentaire.setIdCommentaire(rs.getInt("idCommentaire"));
                    commentaire.setIdUtilisateur(rs.getInt("idUtilisateur"));
                    commentaire.setIdEvenement(rs.getInt("idEvenement"));
                    commentaire.setContenu(rs.getString("contenu"));
                    commentaire.setDateHeureCommentaire(rs.getTimestamp("dateheure").toLocalDateTime());
                  
                    commentaires.add(commentaire);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                connexionDAO.dbClose();
            }
            return commentaires;
        }

        // Méthode pour ajouter un commentaire
        public boolean addCommentaire(Commentaire commentaire) {
            Connection con = null;
            PreparedStatement ps = null;
            boolean success=false;

            try {
                connexionDAO.dbConnect();
                con = connexionDAO.conn;
                ps = con.prepareStatement("INSERT INTO commentaire (idUtilisateur, IdEvenement, contenu,dateHeure) VALUES (?, ?, ?,?)");
               
                ps.setInt(2, commentaire.getIdEvenement());
                ps.setInt(1, commentaire.getIdUtilisateur());
                ps.setString(3, commentaire.getContenu());
                LocalDateTime now = LocalDateTime.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                String formattedDateTime = now.format(formatter);
                ps.setString(4, formattedDateTime);
                
                int rowsAffected = ps.executeUpdate();
                success = (rowsAffected > 0);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                connexionDAO.dbClose();
            }
            return success;
        }

    public int getId(String mail) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int returnValue = 0;

        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
            ps = con.prepareStatement("SELECT idusers FROM users WHERE email=?");
            ps.setString(1, mail);
            rs = ps.executeQuery();
            if (rs.next()) {
            	returnValue=rs.getInt("idusers");
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connexionDAO.dbClose();
        }
        return returnValue;
    }

    public Evenement get(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Evenement evenement = new Evenement();
        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
           
            ps = con.prepareStatement("SELECT * FROM evenement WHERE idEvenement = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();

            if (rs.next()) {
                evenement = new Evenement();
                evenement.setIdEvenement(rs.getInt("idEvenement"));
                evenement.setTitre(rs.getString("titre"));
                evenement.setContenu(rs.getString("contenu"));
                evenement.setImage(rs.getBytes("image"));
                evenement.setDateHeure(rs.getTimestamp("dateHeure").toLocalDateTime());
                evenement.setAuteur(rs.getInt("auteur"));
                return evenement;
            }
            return evenement;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connexionDAO.dbClose();
        }
        return evenement;
    }
    
    public ArrayList<Evenement> getLis(String idm) {
    	Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Evenement> evenements = new ArrayList<>();
        
        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
           
            ps = con.prepareStatement("SELECT evenement.*,users.* FROM evenement INNER JOIN users ON evenement.auteur=users.idusers WHERE users.email=?");
            ps.setString(1, idm);
            rs = ps.executeQuery();

            while (rs.next()) {
                Evenement evenement = new Evenement();
                evenement.setIdEvenement(rs.getInt("idEvenement"));
                evenement.setTitre(rs.getString("titre"));
                evenement.setContenu(rs.getString("contenu"));
                
                evenement.setDateHeure(rs.getTimestamp("dateHeure").toLocalDateTime());
                evenement.setAuteur(rs.getInt("auteur"));
                // Récupérer et convertir l'image en base64
                Blob imageBlob = rs.getBlob("image");
                if (imageBlob != null) {
                    byte[] imageBytes = imageBlob.getBytes(1, (int) imageBlob.length());
                    String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                    evenement.setImage(imageBytes);
                }

                evenements.add(evenement);
            }
            return evenements;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connexionDAO.dbClose();
        }
        return evenements;
    }

    public ArrayList<Evenement> getList() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Evenement> evenements = new ArrayList<>();

        try {
            connexionDAO.dbConnect();
            con = connexionDAO.conn;
            ps = con.prepareStatement("SELECT * FROM evenement");
            rs = ps.executeQuery();

            while (rs.next()) {
                Evenement evenement = new Evenement();
                evenement.setIdEvenement(rs.getInt("idEvenement"));
                evenement.setTitre(rs.getString("titre"));
                evenement.setContenu(rs.getString("contenu"));
                
                evenement.setDateHeure(rs.getTimestamp("dateHeure").toLocalDateTime());
                evenement.setAuteur(rs.getInt("auteur"));
                // Récupérer et convertir l'image en base64
                Blob imageBlob = rs.getBlob("image");
                if (imageBlob != null) {
                    byte[] imageBytes = imageBlob.getBytes(1, (int) imageBlob.length());
                    String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                    evenement.setImage(imageBytes);
                }

                evenements.add(evenement);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connexionDAO.dbClose();
        }
        return evenements;
    }
}
