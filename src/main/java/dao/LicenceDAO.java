package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.*;

public class LicenceDAO extends DBDAO {

	public Licence selFilterFed(String code) {
		dbConnect();
		Licence licence = new Licence();
		if (connected == true) {
		String query = "SELECT SUM(F_1_4_ans) AS total_femmes_1_4,"
				+ "    SUM(F_5_9_ans) AS total_femmes_5_9,"
				+ "    SUM(F_10_14_ans) AS total_femmes_10_14,"
				+ "    SUM(F_15_19_ans) AS total_femmes_15_19,"
				+ "    SUM(F_20_24_ans) AS total_femmes_20_24,"
				+ "    SUM(F_25_29_ans) AS total_femmes_25_29,"
				+ "    SUM(F_30_34_ans) AS total_femmes_30_34,"
				+ "    SUM(F_35_39_ans) AS total_femmes_35_39,"
				+ "    SUM(F_40_44_ans) AS total_femmes_40_44,"
				+ "    SUM(F_45_49_ans) AS total_femmes_45_49,"
				+ "    SUM(F_50_54_ans) AS total_femmes_50_54,"
				+ "    SUM(F_55_59_ans) AS total_femmes_55_59,"
				+ "    SUM(F_60_64_ans) AS total_femmes_60_64,"
				+ "    SUM(F_65_69_ans) AS total_femmes_65_69,"
				+ "    SUM(F_70_74_ans) AS total_femmes_70_74,"
				+ "    SUM(F_75_79_ans) AS total_femmes_75_79,"
				+ "    SUM(F_80_99_ans) AS total_femmes_80_99,"
				+ "    SUM(F_NR) AS total_femmes_nr,"
				+ "    SUM(H_1_4_ans) AS total_hommes_1_4,"
				+ "    SUM(H_5_9_ans) AS total_hommes_5_9,"
				+ "    SUM(H_10_14_ans) AS total_hommes_10_14,"
				+ "    SUM(H_15_19_ans) AS total_hommes_15_19,"
				+ "    SUM(H_20_24_ans) AS total_hommes_20_24,"
				+ "    SUM(H_25_29_ans) AS total_hommes_25_29,"
				+ "    SUM(H_30_34_ans) AS total_hommes_30_34,"
				+ "    SUM(H_35_39_ans) AS total_hommes_35_39,"
				+ "    SUM(H_40_44_ans) AS total_hommes_40_44,"
				+ "    SUM(H_45_49_ans) AS total_hommes_45_49,"
				+ "    SUM(H_50_54_ans) AS total_hommes_50_54,"
				+ "    SUM(H_55_59_ans) AS total_hommes_55_59,"
				+ "    SUM(H_60_64_ans) AS total_hommes_60_64,"
				+ "    SUM(H_65_69_ans) AS total_hommes_65_69,"
				+ "    SUM(H_70_74_ans) AS total_hommes_70_74,"
				+ "    SUM(H_75_79_ans) AS total_hommes_75_79,"
				+ "    SUM(H_80_99_ans) AS total_hommes_80_99,"
				+ "    SUM(H_NR) AS total_hommes_nr,"
				+ "    SUM(NR_NR) AS total_nr_nr,"
				+ "    SUM(Total) AS total_total FROM club_sport.licence where Code=?";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, code);
		ResultSet rs = ps.executeQuery();
	
		if(rs.next()) {
		  
		    licence.setF1_4(rs.getInt("total_femmes_1_4"));
		    licence.setF5_9(rs.getInt("total_femmes_5_9"));
		    licence.setF10_14(rs.getInt("total_femmes_10_14"));
		    licence.setF15_19(rs.getInt("total_femmes_15_19"));
		    licence.setF20_24(rs.getInt("total_femmes_20_24"));
		    licence.setF25_29(rs.getInt("total_femmes_25_29"));
		    licence.setF30_34(rs.getInt("total_femmes_30_34"));
		    licence.setF35_39(rs.getInt("total_femmes_35_39"));
		    licence.setF40_44(rs.getInt("total_femmes_40_44"));
		    licence.setF45_49(rs.getInt("total_femmes_45_49"));
		    licence.setF50_54(rs.getInt("total_femmes_50_54"));
		    licence.setF55_59(rs.getInt("total_femmes_55_59"));
		    licence.setF60_64(rs.getInt("total_femmes_60_64"));
		    licence.setF65_69(rs.getInt("total_femmes_65_69"));
		    licence.setF70_74(rs.getInt("total_femmes_70_74"));
		    licence.setF75_79(rs.getInt("total_femmes_75_79"));
		    licence.setF80_99(rs.getInt("total_femmes_80_99"));
		    licence.setH1_4(rs.getInt("total_hommes_1_4"));
		    licence.setH5_9(rs.getInt("total_hommes_5_9"));
		    licence.setH10_14(rs.getInt("total_hommes_10_14"));
		    licence.setH15_19(rs.getInt("total_hommes_15_19"));
		    licence.setH20_24(rs.getInt("total_hommes_20_24"));
		    licence.setH25_29(rs.getInt("total_hommes_25_29"));
		    licence.setH30_34(rs.getInt("total_hommes_30_34"));
		    licence.setH35_39(rs.getInt("total_hommes_35_39"));
		    licence.setH40_44(rs.getInt("total_hommes_40_44"));
		    licence.setH45_49(rs.getInt("total_hommes_45_49"));
		    licence.setH50_54(rs.getInt("total_hommes_50_54"));
		    licence.setH55_59(rs.getInt("total_hommes_55_59"));
		    licence.setH60_64(rs.getInt("total_hommes_60_64"));
		    licence.setH65_69(rs.getInt("total_hommes_65_69"));
		    licence.setH70_74(rs.getInt("total_hommes_70_74"));
		    licence.setH75_79(rs.getInt("total_hommes_75_79"));
		    licence.setH80_99(rs.getInt("total_hommes_80_99"));
		    licence.setTotal(rs.getInt("total_total"));

		    // Assurez-vous de retourner l'objet Licence
		    return licence;
		}
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return licence;
		}
	
	public Licence selFilterReg(String reg,String code) {
		dbConnect();
		Licence licence = new Licence();
		if (connected == true) {
		String query = "SELECT SUM(F_1_4_ans) AS total_femmes_1_4,"
				+ "    SUM(F_5_9_ans) AS total_femmes_5_9,"
				+ "    SUM(F_10_14_ans) AS total_femmes_10_14,"
				+ "    SUM(F_15_19_ans) AS total_femmes_15_19,"
				+ "    SUM(F_20_24_ans) AS total_femmes_20_24,"
				+ "    SUM(F_25_29_ans) AS total_femmes_25_29,"
				+ "    SUM(F_30_34_ans) AS total_femmes_30_34,"
				+ "    SUM(F_35_39_ans) AS total_femmes_35_39,"
				+ "    SUM(F_40_44_ans) AS total_femmes_40_44,"
				+ "    SUM(F_45_49_ans) AS total_femmes_45_49,"
				+ "    SUM(F_50_54_ans) AS total_femmes_50_54,"
				+ "    SUM(F_55_59_ans) AS total_femmes_55_59,"
				+ "    SUM(F_60_64_ans) AS total_femmes_60_64,"
				+ "    SUM(F_65_69_ans) AS total_femmes_65_69,"
				+ "    SUM(F_70_74_ans) AS total_femmes_70_74,"
				+ "    SUM(F_75_79_ans) AS total_femmes_75_79,"
				+ "    SUM(F_80_99_ans) AS total_femmes_80_99,"
				+ "    SUM(F_NR) AS total_femmes_nr,"
				+ "    SUM(H_1_4_ans) AS total_hommes_1_4,"
				+ "    SUM(H_5_9_ans) AS total_hommes_5_9,"
				+ "    SUM(H_10_14_ans) AS total_hommes_10_14,"
				+ "    SUM(H_15_19_ans) AS total_hommes_15_19,"
				+ "    SUM(H_20_24_ans) AS total_hommes_20_24,"
				+ "    SUM(H_25_29_ans) AS total_hommes_25_29,"
				+ "    SUM(H_30_34_ans) AS total_hommes_30_34,"
				+ "    SUM(H_35_39_ans) AS total_hommes_35_39,"
				+ "    SUM(H_40_44_ans) AS total_hommes_40_44,"
				+ "    SUM(H_45_49_ans) AS total_hommes_45_49,"
				+ "    SUM(H_50_54_ans) AS total_hommes_50_54,"
				+ "    SUM(H_55_59_ans) AS total_hommes_55_59,"
				+ "    SUM(H_60_64_ans) AS total_hommes_60_64,"
				+ "    SUM(H_65_69_ans) AS total_hommes_65_69,"
				+ "    SUM(H_70_74_ans) AS total_hommes_70_74,"
				+ "    SUM(H_75_79_ans) AS total_hommes_75_79,"
				+ "    SUM(H_80_99_ans) AS total_hommes_80_99,"
				+ "    SUM(H_NR) AS total_hommes_nr,"
				+ "    SUM(NR_NR) AS total_nr_nr,"
				+ "    SUM(Total) AS total_total FROM club_sport.licence where Region=? and Code=?";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, reg);
		ps.setString(2, code);
		ResultSet rs = ps.executeQuery();
	
		if(rs.next()) {
			
			licence.setF1_4(rs.getInt("total_femmes_1_4"));
		    licence.setF5_9(rs.getInt("total_femmes_5_9"));
		    licence.setF10_14(rs.getInt("total_femmes_10_14"));
		    licence.setF15_19(rs.getInt("total_femmes_15_19"));
		    licence.setF20_24(rs.getInt("total_femmes_20_24"));
		    licence.setF25_29(rs.getInt("total_femmes_25_29"));
		    licence.setF30_34(rs.getInt("total_femmes_30_34"));
		    licence.setF35_39(rs.getInt("total_femmes_35_39"));
		    licence.setF40_44(rs.getInt("total_femmes_40_44"));
		    licence.setF45_49(rs.getInt("total_femmes_45_49"));
		    licence.setF50_54(rs.getInt("total_femmes_50_54"));
		    licence.setF55_59(rs.getInt("total_femmes_55_59"));
		    licence.setF60_64(rs.getInt("total_femmes_60_64"));
		    licence.setF65_69(rs.getInt("total_femmes_65_69"));
		    licence.setF70_74(rs.getInt("total_femmes_70_74"));
		    licence.setF75_79(rs.getInt("total_femmes_75_79"));
		    licence.setF80_99(rs.getInt("total_femmes_80_99"));
		    licence.setH1_4(rs.getInt("total_hommes_1_4"));
		    licence.setH5_9(rs.getInt("total_hommes_5_9"));
		    licence.setH10_14(rs.getInt("total_hommes_10_14"));
		    licence.setH15_19(rs.getInt("total_hommes_15_19"));
		    licence.setH20_24(rs.getInt("total_hommes_20_24"));
		    licence.setH25_29(rs.getInt("total_hommes_25_29"));
		    licence.setH30_34(rs.getInt("total_hommes_30_34"));
		    licence.setH35_39(rs.getInt("total_hommes_35_39"));
		    licence.setH40_44(rs.getInt("total_hommes_40_44"));
		    licence.setH45_49(rs.getInt("total_hommes_45_49"));
		    licence.setH50_54(rs.getInt("total_hommes_50_54"));
		    licence.setH55_59(rs.getInt("total_hommes_55_59"));
		    licence.setH60_64(rs.getInt("total_hommes_60_64"));
		    licence.setH65_69(rs.getInt("total_hommes_65_69"));
		    licence.setH70_74(rs.getInt("total_hommes_70_74"));
		    licence.setH75_79(rs.getInt("total_hommes_75_79"));
		    licence.setH80_99(rs.getInt("total_hommes_80_99"));
		    licence.setTotal(rs.getInt("total_total"));

		    // Assurez-vous de retourner l'objet Licence
		    return licence;
		}
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return licence;
		}
	
	public Licence selFilterCom(String com, String code) {
		dbConnect();
		Licence licence = new Licence();
		if (connected == true) {
		String query = "SELECT SUM(F_1_4_ans) AS total_femmes_1_4,"
				+ "    SUM(F_5_9_ans) AS total_femmes_5_9,"
				+ "    SUM(F_10_14_ans) AS total_femmes_10_14,"
				+ "    SUM(F_15_19_ans) AS total_femmes_15_19,"
				+ "    SUM(F_20_24_ans) AS total_femmes_20_24,"
				+ "    SUM(F_25_29_ans) AS total_femmes_25_29,"
				+ "    SUM(F_30_34_ans) AS total_femmes_30_34,"
				+ "    SUM(F_35_39_ans) AS total_femmes_35_39,"
				+ "    SUM(F_40_44_ans) AS total_femmes_40_44,"
				+ "    SUM(F_45_49_ans) AS total_femmes_45_49,"
				+ "    SUM(F_50_54_ans) AS total_femmes_50_54,"
				+ "    SUM(F_55_59_ans) AS total_femmes_55_59,"
				+ "    SUM(F_60_64_ans) AS total_femmes_60_64,"
				+ "    SUM(F_65_69_ans) AS total_femmes_65_69,"
				+ "    SUM(F_70_74_ans) AS total_femmes_70_74,"
				+ "    SUM(F_75_79_ans) AS total_femmes_75_79,"
				+ "    SUM(F_80_99_ans) AS total_femmes_80_99,"
				+ "    SUM(F_NR) AS total_femmes_nr,"
				+ "    SUM(H_1_4_ans) AS total_hommes_1_4,"
				+ "    SUM(H_5_9_ans) AS total_hommes_5_9,"
				+ "    SUM(H_10_14_ans) AS total_hommes_10_14,"
				+ "    SUM(H_15_19_ans) AS total_hommes_15_19,"
				+ "    SUM(H_20_24_ans) AS total_hommes_20_24,"
				+ "    SUM(H_25_29_ans) AS total_hommes_25_29,"
				+ "    SUM(H_30_34_ans) AS total_hommes_30_34,"
				+ "    SUM(H_35_39_ans) AS total_hommes_35_39,"
				+ "    SUM(H_40_44_ans) AS total_hommes_40_44,"
				+ "    SUM(H_45_49_ans) AS total_hommes_45_49,"
				+ "    SUM(H_50_54_ans) AS total_hommes_50_54,"
				+ "    SUM(H_55_59_ans) AS total_hommes_55_59,"
				+ "    SUM(H_60_64_ans) AS total_hommes_60_64,"
				+ "    SUM(H_65_69_ans) AS total_hommes_65_69,"
				+ "    SUM(H_70_74_ans) AS total_hommes_70_74,"
				+ "    SUM(H_75_79_ans) AS total_hommes_75_79,"
				+ "    SUM(H_80_99_ans) AS total_hommes_80_99,"
				+ "    SUM(H_NR) AS total_hommes_nr,"
				+ "    SUM(NR_NR) AS total_nr_nr,"
				+ "    SUM(Total) AS total_total FROM club_sport.licence where Commune=? and Code=?";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, com);
		ps.setString(2, code);
		ResultSet rs = ps.executeQuery();
	
		if(rs.next()) {
			
			licence.setF1_4(rs.getInt("total_femmes_1_4"));
		    licence.setF5_9(rs.getInt("total_femmes_5_9"));
		    licence.setF10_14(rs.getInt("total_femmes_10_14"));
		    licence.setF15_19(rs.getInt("total_femmes_15_19"));
		    licence.setF20_24(rs.getInt("total_femmes_20_24"));
		    licence.setF25_29(rs.getInt("total_femmes_25_29"));
		    licence.setF30_34(rs.getInt("total_femmes_30_34"));
		    licence.setF35_39(rs.getInt("total_femmes_35_39"));
		    licence.setF40_44(rs.getInt("total_femmes_40_44"));
		    licence.setF45_49(rs.getInt("total_femmes_45_49"));
		    licence.setF50_54(rs.getInt("total_femmes_50_54"));
		    licence.setF55_59(rs.getInt("total_femmes_55_59"));
		    licence.setF60_64(rs.getInt("total_femmes_60_64"));
		    licence.setF65_69(rs.getInt("total_femmes_65_69"));
		    licence.setF70_74(rs.getInt("total_femmes_70_74"));
		    licence.setF75_79(rs.getInt("total_femmes_75_79"));
		    licence.setF80_99(rs.getInt("total_femmes_80_99"));
		    licence.setH1_4(rs.getInt("total_hommes_1_4"));
		    licence.setH5_9(rs.getInt("total_hommes_5_9"));
		    licence.setH10_14(rs.getInt("total_hommes_10_14"));
		    licence.setH15_19(rs.getInt("total_hommes_15_19"));
		    licence.setH20_24(rs.getInt("total_hommes_20_24"));
		    licence.setH25_29(rs.getInt("total_hommes_25_29"));
		    licence.setH30_34(rs.getInt("total_hommes_30_34"));
		    licence.setH35_39(rs.getInt("total_hommes_35_39"));
		    licence.setH40_44(rs.getInt("total_hommes_40_44"));
		    licence.setH45_49(rs.getInt("total_hommes_45_49"));
		    licence.setH50_54(rs.getInt("total_hommes_50_54"));
		    licence.setH55_59(rs.getInt("total_hommes_55_59"));
		    licence.setH60_64(rs.getInt("total_hommes_60_64"));
		    licence.setH65_69(rs.getInt("total_hommes_65_69"));
		    licence.setH70_74(rs.getInt("total_hommes_70_74"));
		    licence.setH75_79(rs.getInt("total_hommes_75_79"));
		    licence.setH80_99(rs.getInt("total_hommes_80_99"));
		    licence.setTotal(rs.getInt("total_total"));

		    // Assurez-vous de retourner l'objet Licence
		    return licence;
		}
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return licence;
		}
	
	public Licence selFilterDep(String dep, String code) {
		dbConnect();
		Licence licence = new Licence();
		if (connected == true) {
		String query = "SELECT SUM(F_1_4_ans) AS total_femmes_1_4,"
				+ "    SUM(F_5_9_ans) AS total_femmes_5_9,"
				+ "    SUM(F_10_14_ans) AS total_femmes_10_14,"
				+ "    SUM(F_15_19_ans) AS total_femmes_15_19,"
				+ "    SUM(F_20_24_ans) AS total_femmes_20_24,"
				+ "    SUM(F_25_29_ans) AS total_femmes_25_29,"
				+ "    SUM(F_30_34_ans) AS total_femmes_30_34,"
				+ "    SUM(F_35_39_ans) AS total_femmes_35_39,"
				+ "    SUM(F_40_44_ans) AS total_femmes_40_44,"
				+ "    SUM(F_45_49_ans) AS total_femmes_45_49,"
				+ "    SUM(F_50_54_ans) AS total_femmes_50_54,"
				+ "    SUM(F_55_59_ans) AS total_femmes_55_59,"
				+ "    SUM(F_60_64_ans) AS total_femmes_60_64,"
				+ "    SUM(F_65_69_ans) AS total_femmes_65_69,"
				+ "    SUM(F_70_74_ans) AS total_femmes_70_74,"
				+ "    SUM(F_75_79_ans) AS total_femmes_75_79,"
				+ "    SUM(F_80_99_ans) AS total_femmes_80_99,"
				+ "    SUM(F_NR) AS total_femmes_nr,"
				+ "    SUM(H_1_4_ans) AS total_hommes_1_4,"
				+ "    SUM(H_5_9_ans) AS total_hommes_5_9,"
				+ "    SUM(H_10_14_ans) AS total_hommes_10_14,"
				+ "    SUM(H_15_19_ans) AS total_hommes_15_19,"
				+ "    SUM(H_20_24_ans) AS total_hommes_20_24,"
				+ "    SUM(H_25_29_ans) AS total_hommes_25_29,"
				+ "    SUM(H_30_34_ans) AS total_hommes_30_34,"
				+ "    SUM(H_35_39_ans) AS total_hommes_35_39,"
				+ "    SUM(H_40_44_ans) AS total_hommes_40_44,"
				+ "    SUM(H_45_49_ans) AS total_hommes_45_49,"
				+ "    SUM(H_50_54_ans) AS total_hommes_50_54,"
				+ "    SUM(H_55_59_ans) AS total_hommes_55_59,"
				+ "    SUM(H_60_64_ans) AS total_hommes_60_64,"
				+ "    SUM(H_65_69_ans) AS total_hommes_65_69,"
				+ "    SUM(H_70_74_ans) AS total_hommes_70_74,"
				+ "    SUM(H_75_79_ans) AS total_hommes_75_79,"
				+ "    SUM(H_80_99_ans) AS total_hommes_80_99,"
				+ "    SUM(H_NR) AS total_hommes_nr,"
				+ "    SUM(NR_NR) AS total_nr_nr,"
				+ "    SUM(Total) AS total_total FROM club_sport.licence where Departement=? and Code=?";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, dep);
		ps.setString(2, code);
		ResultSet rs = ps.executeQuery();
	
		if(rs.next()) {
		
			licence.setF1_4(rs.getInt("total_femmes_1_4"));
		    licence.setF5_9(rs.getInt("total_femmes_5_9"));
		    licence.setF10_14(rs.getInt("total_femmes_10_14"));
		    licence.setF15_19(rs.getInt("total_femmes_15_19"));
		    licence.setF20_24(rs.getInt("total_femmes_20_24"));
		    licence.setF25_29(rs.getInt("total_femmes_25_29"));
		    licence.setF30_34(rs.getInt("total_femmes_30_34"));
		    licence.setF35_39(rs.getInt("total_femmes_35_39"));
		    licence.setF40_44(rs.getInt("total_femmes_40_44"));
		    licence.setF45_49(rs.getInt("total_femmes_45_49"));
		    licence.setF50_54(rs.getInt("total_femmes_50_54"));
		    licence.setF55_59(rs.getInt("total_femmes_55_59"));
		    licence.setF60_64(rs.getInt("total_femmes_60_64"));
		    licence.setF65_69(rs.getInt("total_femmes_65_69"));
		    licence.setF70_74(rs.getInt("total_femmes_70_74"));
		    licence.setF75_79(rs.getInt("total_femmes_75_79"));
		    licence.setF80_99(rs.getInt("total_femmes_80_99"));
		    licence.setH1_4(rs.getInt("total_hommes_1_4"));
		    licence.setH5_9(rs.getInt("total_hommes_5_9"));
		    licence.setH10_14(rs.getInt("total_hommes_10_14"));
		    licence.setH15_19(rs.getInt("total_hommes_15_19"));
		    licence.setH20_24(rs.getInt("total_hommes_20_24"));
		    licence.setH25_29(rs.getInt("total_hommes_25_29"));
		    licence.setH30_34(rs.getInt("total_hommes_30_34"));
		    licence.setH35_39(rs.getInt("total_hommes_35_39"));
		    licence.setH40_44(rs.getInt("total_hommes_40_44"));
		    licence.setH45_49(rs.getInt("total_hommes_45_49"));
		    licence.setH50_54(rs.getInt("total_hommes_50_54"));
		    licence.setH55_59(rs.getInt("total_hommes_55_59"));
		    licence.setH60_64(rs.getInt("total_hommes_60_64"));
		    licence.setH65_69(rs.getInt("total_hommes_65_69"));
		    licence.setH70_74(rs.getInt("total_hommes_70_74"));
		    licence.setH75_79(rs.getInt("total_hommes_75_79"));
		    licence.setH80_99(rs.getInt("total_hommes_80_99"));
		    licence.setTotal(rs.getInt("total_total"));

		    // Assurez-vous de retourner l'objet Licence
	    return licence;
		}
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return licence;
		}


}
