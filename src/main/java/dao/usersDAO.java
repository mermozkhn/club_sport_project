package dao;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.mindrot.jbcrypt.BCrypt;

import jakarta.servlet.annotation.WebServlet;
import model.Utilisateur;



public class usersDAO extends DBDAO {

    public usersDAO() {
        super();
    }

    public boolean addUser(String nom, String prenom, String email, String motdepass, String profil, String fileName, InputStream fileContent, String domaine, String value) {
        PreparedStatement stmt = null;
        try {
            dbConnect();
            if (!connected) {
                System.err.println("La connexion à la base de données n'a pas pu être établie.");
                return false;
            }

            String query = "INSERT INTO users (nom, prenom, email, motdepass, profil, fileName, file, domaine, fede, validate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(query);
            stmt.setString(1, nom);
            stmt.setString(2, prenom);
            stmt.setString(3, email);
            stmt.setString(4, motdepass);
            stmt.setString(5, profil);
            stmt.setString(6, fileName);
            if (fileContent != null) {
                stmt.setBlob(7, fileContent);
            }
            stmt.setString(8, domaine);
            stmt.setString(9, value);
            stmt.setString(10, "false");
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            System.err.println("Erreur lors de l'ajout de l'utilisateur : " + e.getMessage());
            e.printStackTrace();
            return false;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    System.err.println("Erreur lors de la fermeture de la session : " + e.getMessage());
                }
            }
            dbClose();
        }
    }

    public boolean addUse(String nom, String prenom, String email, String motdepass, String profil, String fileName, InputStream fileContent, String domaine, String value) {
        PreparedStatement stmt = null;
        try {
            dbConnect();
            if (!connected) {
                System.err.println("La connexion à la base de données n'a pas pu être établie.");
                return false;
            }

            String query = "INSERT INTO users (nom, prenom, email, motdepass, profil, fileName, file, domaine, fede, validate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(query);
            stmt.setString(1, nom);
            stmt.setString(2, prenom);
            stmt.setString(3, email);
            stmt.setString(4, motdepass);
            stmt.setString(5, profil);
            stmt.setString(6, fileName);
            if (fileContent != null) {
                stmt.setBlob(7, fileContent);
            }else {
            	stmt.setBlob(7, fileContent);
            }
            stmt.setString(8, domaine);
            stmt.setString(9, value);
            stmt.setString(10, "true");
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            System.err.println("Erreur lors de l'ajout de l'utilisateur : " + e.getMessage());
            e.printStackTrace();
            return false;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    System.err.println("Erreur lors de la fermeture de la session : " + e.getMessage());
                }
            }
            dbClose();
        }
    }

    
    // Modifie cette méthode pour retourner un String au lieu de boolean
    public Utilisateur authenticateUser(String email, String motdepass) {
        PreparedStatement stmt = null;
        String pass="";
        Utilisateur use = new Utilisateur();
        ResultSet rs = null;
        try {
            dbConnect();
            String query = "SELECT * FROM users WHERE email = ? AND validate = ?";
            stmt = conn.prepareStatement(query);
            stmt.setString(1, email);
            stmt.setString(2, "true");
            rs = stmt.executeQuery();
            if (rs.next()) {
            	pass=rs.getString("motdepass");
            	Boolean isPasswordCorrect = checkPassword(motdepass,pass);
            	if(isPasswordCorrect) {
            		use.setId_user(rs.getInt("idusers"));
            		use.setE_mail(rs.getString("email"));
            		use.setNom(rs.getString("nom"));
            		use.setPrenom(rs.getString("prenom"));
            		use.setProfil(rs.getString("profil"));
            		return use; // Retourne le nom de l'utilisateur
            	}
            }
            return null; // Retourne null si l'utilisateur n'est pas trouvé
        } catch (SQLException e) {
            System.err.println("Erreur d'authentification : " + e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (rs != null) rs.close();
               if (stmt != null) stmt.close();
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture : " + e.getMessage());
            }
        }
    }
    
    // Méthode pour vérifier un mot de passe avec son hash
    public static boolean checkPassword(String plainPassword, String hashedPassword) {
        return BCrypt.checkpw(plainPassword, hashedPassword);
    }
}
