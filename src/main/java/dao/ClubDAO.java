/**
 * 
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.CalculeDistance;
import model.*;
/**
 * @author Mermoz KANHONOU
 *
 */
public class ClubDAO extends DBDAO{

	public ArrayList<Federation> findFed() {
		dbConnect();
		ArrayList<Federation> list = new ArrayList<>();
		
		if (connected == true) {
		String query = "SELECT DISTINCT code_fede, federation FROM club_sport.clubs";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
	
		 while (rs.next()) {
             Federation fed = new Federation();
             fed.setCode(rs.getString("code_fede"));
             fed.setFede(rs.getString("federation"));
             list.add(fed);
         }
		return list;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return list;
		}
	
	public String fed(String co) {
		dbConnect();
		String fe="";
		
		if (connected == true) {
		String query = "SELECT federation FROM club_sport.clubs where code_fede=?";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, co);
		ResultSet rs = ps.executeQuery();
	
		 while (rs.next()) {
             
             fe=rs.getString("federation");
         }
		return fe;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return fe;
		}
	
	public ArrayList<String> findDep() {
		dbConnect();
		ArrayList<String> list = new ArrayList<>();
		
		if (connected == true) {
		String query = "SELECT DISTINCT Departement FROM club_sport.licence";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
	
		 while (rs.next()) {
             String fed;
             fed=rs.getString("Departement");
             list.add(fed);
         }
		return list;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return list;
		}
	
	public ArrayList<String> findCom() {
		dbConnect();
		ArrayList<String> list = new ArrayList<>();
		
		if (connected == true) {
		String query = "SELECT DISTINCT Commune FROM club_sport.licence";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
	
		 while (rs.next()) {
             String fed;
             fed=rs.getString("Commune");
             list.add(fed);
         }
		return list;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return list;
		}
	
	public ArrayList<String> findCo(String quer) {
		dbConnect();
		ArrayList<String> list = new ArrayList<>();
		
		if (connected == true) {
		String query = "SELECT DISTINCT Commune FROM club_sport.licence WHERE Commune LIKE ? LIMIT 20";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1,"%" + quer + "%");
		ResultSet rs = ps.executeQuery();
	
		 while (rs.next()) {
             String fed;
             fed=rs.getString("Commune");
             list.add(fed);
         }
		return list;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return list;
		}
	
	public ArrayList<String> findRegion() {
		dbConnect();
		ArrayList<String> list = new ArrayList<>();
		if (connected == true) {
		String query = "SELECT distinct region FROM club_sport.clubs";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
	
		while(rs.next()) {
			list.add(rs.getString("region"));
		}
		return list;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		list.add("N");
		return list;
		}
	
	public Clubs selFilterCommune(String code, String commune) {
		dbConnect();
		Clubs club = new Clubs();
		if (connected == true) {
		String query = "SELECT * FROM club_sport.clubs WHERE code_fede=? AND code_commune =? ";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, commune);
		ps.setString(2, code);
		ResultSet rs = ps.executeQuery();
	
		if(rs.next()) {
			club.setCode_commune(rs.getString("code_commune"));
			club.setClub(rs.getString("club"));
			club.setCode_federation(rs.getString("code_fede"));
			club.setDepartement(rs.getString("departement"));
			club.setEpa(rs.getString("epa"));
			club.setId_club(rs.getInt("idclubs"));
			club.setNom_federation(rs.getString("federation"));
			club.setRegion(rs.getString("region"));
			club.setTotal_club(rs.getString("total"));

			return club;
		}
		//else {
			//club.setRegion("Nothing");
		//}
		
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return club;
		}
	public List<Clubs> selFilterCommuneAlone(String commune) {
		dbConnect();
		List<Clubs> clu = new ArrayList<>();
		
		if (connected == true) {
		String query = "SELECT * FROM club_sport.clubs WHERE code_commune =? ";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, commune);
		ResultSet rs = ps.executeQuery();
	
		while(rs.next()) {
			Clubs club = new Clubs();
			club.setCode_commune(rs.getString("code_commune"));
			club.setClub(rs.getString("club"));
			club.setCode_federation(rs.getString("code_fede"));
			club.setDepartement(rs.getString("departement"));
			club.setEpa(rs.getString("epa"));
			club.setId_club(rs.getInt("idclubs"));
			club.setNom_federation(rs.getString("federation"));
			club.setRegion(rs.getString("region"));
			club.setTotal_club(rs.getString("total"));

			clu.add(club);
			
		}
		return clu;
		
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return clu;
		}
	
	public ArrayList<Clubs> selFilterRegion(String code, String region) {
	    dbConnect();
	    ArrayList<Clubs> list = new ArrayList<>();
	    
	    if (connected == true) {
	        String query = "SELECT * FROM club_sport.clubs WHERE code_fede=? AND region =? Limit 50";
	        try {
	            PreparedStatement ps = conn.prepareStatement(query);
	            ps.setString(1, code);
	            ps.setString(2, region);
	            ResultSet rs = ps.executeQuery();

	            while (rs.next()) {
	                Clubs club = new Clubs(); // Créez une nouvelle instance de Clubs à chaque itération
	                club.setCode_commune(rs.getString("code_commune"));
	                club.setClub(rs.getString("club"));
	                club.setCode_federation(rs.getString("code_fede"));
	                club.setDepartement(rs.getString("departement"));
	                club.setEpa(rs.getString("epa"));
	                club.setId_club(rs.getInt("idclubs"));
	                club.setNom_federation(rs.getString("federation"));
	                club.setRegion(rs.getString("region"));
	                club.setTotal_club(rs.getString("total"));
	                list.add(club);
	            }
	            return list;
	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            dbClose();
	        }
	    }
	    return list;
	}
	
	public List<Clubs> selFilterRegionAlone(String region) {
	    dbConnect();
	    List<Clubs> list = new ArrayList<>();
	    
	    if (connected == true) {
	        String query = "SELECT * FROM club_sport.clubs WHERE region =? LIMIT 300";
	        try {
	            PreparedStatement ps = conn.prepareStatement(query);
	            ps.setString(1, region);
	            ResultSet rs = ps.executeQuery();

	            while (rs.next()) {
	                Clubs club = new Clubs(); // Créez une nouvelle instance de Clubs à chaque itération
	                club.setCode_commune(rs.getString("code_commune"));
	                club.setClub(rs.getString("club"));
	                club.setCode_federation(rs.getString("code_fede"));
	                club.setDepartement(rs.getString("departement"));
	                club.setEpa(rs.getString("epa"));
	                club.setId_club(rs.getInt("idclubs"));
	                club.setNom_federation(rs.getString("federation"));
	                club.setRegion(rs.getString("region"));
	                club.setTotal_club(rs.getString("total"));
	                list.add(club);
	            }
	            return list;
	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            dbClose();
	        }
	    }
	    return list;
	}

	
	public ArrayList<String> findByPostal(String code) {
		dbConnect();
		ArrayList<String> list = new ArrayList<>();
		if (connected == true) {
		String query = "SELECT distinct insee_code FROM club_sport.communes WHERE codepostal=?";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, code);
		ResultSet rs = ps.executeQuery();
	
		while(rs.next()) {
			list.add(rs.getString("insee_code"));
		}
		return list;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return list;
		}
	
	public Commune keepCommune(String code,String code_fede) {
		dbConnect();
		Commune club = new Commune();
		if (connected == true) {
		String query = "SELECT communes.*, licence.* FROM communes INNER JOIN ( SELECT Code_Commune, SUM(F_1_4_ans + F_5_9_ans + F_10_14_ans + F_15_19_ans + F_20_24_ans + F_25_29_ans + F_30_34_ans + F_35_39_ans + F_40_44_ans + F_45_49_ans + F_50_54_ans + F_55_59_ans + F_60_64_ans + F_65_69_ans + F_70_74_ans + F_75_79_ans + F_80_99_ans + F_NR) AS total_femmes, SUM(H_1_4_ans + H_5_9_ans + H_10_14_ans + H_15_19_ans + H_20_24_ans + H_25_29_ans + H_30_34_ans + H_35_39_ans + H_40_44_ans + H_45_49_ans + H_50_54_ans + H_55_59_ans + H_60_64_ans + H_65_69_ans + H_70_74_ans + H_75_79_ans + H_80_99_ans + H_NR) AS total_hommes, SUM(Total) AS Total, SUM(NR_NR) AS NR_NR FROM licence WHERE licence.Code_Commune =? AND licence.Code =? GROUP BY Code_Commune ) AS licence ON communes.insee_code = licence.Code_Commune";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, code);
		ps.setString(2, code_fede);
		ResultSet rs = ps.executeQuery();
	
		if(rs.next()) {
			club.setNomCommune(rs.getString("nomcommune"));
			club.setDepartmentName(rs.getString("department_name"));
			club.setCodePostal(rs.getString("codepostal"));
			club.setDepartmentNumber(rs.getString("department_number"));
			club.setInseeCode(rs.getString("insee_code"));
			club.setLabel(rs.getString("NR_NR"));
			club.setLatitude(rs.getString("latitude"));
			club.setLongitude(rs.getString("longitude"));
			club.setRegionName(rs.getString("region_geojson_name"));
			club.setRegionGeoJsonName(rs.getString("Total"));
			club.setFemm(rs.getString("total_femmes"));
			club.setHomm(rs.getString("total_hommes"));
			
		}
		else {
			club=null;
		}
		//return club;
		
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return club;
		}
	
	public Commune keepCommuneAlone(String code) {
		dbConnect();
		Commune club = new Commune();
		if (connected == true) {
		String query = "SELECT communes.*, licence.* FROM communes INNER JOIN ( SELECT Code_Commune, SUM(F_1_4_ans + F_5_9_ans + F_10_14_ans + F_15_19_ans + F_20_24_ans + F_25_29_ans + F_30_34_ans + F_35_39_ans + F_40_44_ans + F_45_49_ans + F_50_54_ans + F_55_59_ans + F_60_64_ans + F_65_69_ans + F_70_74_ans + F_75_79_ans + F_80_99_ans + F_NR) AS total_femmes, SUM(H_1_4_ans + H_5_9_ans + H_10_14_ans + H_15_19_ans + H_20_24_ans + H_25_29_ans + H_30_34_ans + H_35_39_ans + H_40_44_ans + H_45_49_ans + H_50_54_ans + H_55_59_ans + H_60_64_ans + H_65_69_ans + H_70_74_ans + H_75_79_ans + H_80_99_ans + H_NR) AS total_hommes, SUM(Total) AS Total, SUM(NR_NR) AS NR_NR FROM licence WHERE licence.Code_Commune =? GROUP BY Code_Commune ) AS licence ON communes.insee_code = licence.Code_Commune";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, code);
		ResultSet rs = ps.executeQuery();
	
		if(rs.next()) {
			club.setNomCommune(rs.getString("nomcommune"));
			club.setDepartmentName(rs.getString("department_name"));
			club.setCodePostal(rs.getString("codepostal"));
			club.setDepartmentNumber(rs.getString("department_number"));
			club.setInseeCode(rs.getString("insee_code"));
			club.setLabel(rs.getString("NR_NR"));
			club.setLatitude(rs.getString("latitude"));
			club.setLongitude(rs.getString("longitude"));
			club.setRegionName(rs.getString("region_geojson_name"));
			club.setRegionGeoJsonName(rs.getString("Total"));
			club.setFemm(rs.getString("total_femmes"));
			club.setHomm(rs.getString("total_hommes"));
			
		}
		else {
			club=null;
		}
		//return club;
		
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return club;
		}
	
	
	public ArrayList<Commune> selectByRayon(Double code, Double lat, Double lon) {
		dbConnect();
		ArrayList<Commune> clu = new ArrayList<>();
		
		if (connected == true) {
		String query = "SELECT * FROM club_sport.communes";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
	
		while(rs.next()) {
			Commune club = new Commune();
			club.setNomCommune(rs.getString("nomcommune"));
			club.setDepartmentName(rs.getString("department_name"));
			club.setCodePostal(rs.getString("codepostal"));
			club.setDepartmentNumber(rs.getString("department_number"));
			club.setInseeCode(rs.getString("insee_code"));
			club.setLabel(rs.getString("label"));
			club.setLatitude(rs.getString("latitude"));
			club.setLongitude(rs.getString("longitude"));
			club.setRegionName(rs.getString("region_name"));
			
			double lati = Double.parseDouble(club.getLatitude());
			double longi = Double.parseDouble(club.getLongitude());
			double distance = CalculeDistance.calculateDistance(lat,lon,lati,longi);                
			//Ajout de la distance dans station
			club.setRegionGeoJsonName(Double.toString(distance));

			// Si la station est dans le rayon, on l'ajoute à resultats
			if(distance < code) {
				clu.add(club);
			}
		}
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return clu;
		}
	
}
