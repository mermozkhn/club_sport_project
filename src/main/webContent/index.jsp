<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import= "controllers.*" %>
<%@ page import= "model.*" %>
<%@ page import= "dao.*" %>
<%@ page import= "java.io.IOException"%>
<%@ page import= " java.io.PrintWriter"%>
<%@ page import = "java.util.*"%>
<%@ page import = "jakarta.servlet.http.HttpSession"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<title>Gouv'Seekers</title>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />
<!-- Style CSS pour définir la hauteur de la carte -->
<style>
	   body{
	   position: relative;
	   }
	   
        body::before {
            content: '';
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('./backk.jpg');
            background-size: cover;
            background-repeat: repeat;
            filter: blur(7px);
            z-index: -1;
        }
  .content {
            position: relative;
            z-index: 1;
            padding: 20px;
            
            text-align: center; /* Centrer le contenu horizontalement */
        }
        #map {
            position: relative;
            z-index: 1;
            padding: 20px;
            height: 500px;
            text-align: center; /* Centrer le contenu horizontalement */
        }

</style>
</head>
<body>
<nav class="navbar bg-body-tertiary">
  <div class="container-fluid">
    <a class="navbar-brand btn btn-outline-secondary" href="index.jsp">Accueil</a>
   <%
   String noom = "";
    HttpSession sessi = request.getSession(false);
    if (sessi != null) {
        Utilisateur nome = (Utilisateur) sessi.getAttribute("userName");
        if (nome != null) {
        	noom = nome.getnom();
            String pro = nome.getprofil();
            if (!pro.equals("Lambda")) {
    %>
                <a class="btn btn-outline-primary" href="NewFile.jsp">Statistiques</a>
                <a class="btn btn-outline-primary" href="Success.jsp">Mes Posts</a>
                <a class="btn btn-outline-danger" href="Logout">Deconnexion</a>
    <%
            } 
            else {
            	  %>
                         <a class="btn btn-outline-primary" href="Actualite.jsp">Actualite</a>
                         <a class="btn btn-outline-danger" href="Logout">Deconnexion</a>
      <%  
                        }
        }
        else {
            %>
                        <a class="btn btn-outline-primary" href="Actualite.jsp">Actualite</a>
                        <a class="btn btn-outline-success" href="Connect.jsp">Compte</a>
            <%
                    }
    }
   
    %>  
  </div>
</nav>

<div class="container">
<div class="content text-center">

<h1>Bienvenue sur notre site <%=noom %></h1>
<script>
        function afficherChamps() {
            var typeRecherche = document.getElementById("type_recherche").value;
            var inputCritere = document.getElementById("critere_input");
            var selectRegion = document.getElementById("region_select");
            var selectRayon = document.getElementById("critere_rayon");

            if (typeRecherche === "code_postal") {
                inputCritere.style.display = "block";
                selectRegion.style.display = "none";
                selectRayon.style.display = "none";
            } else if (typeRecherche === "region") {
                inputCritere.style.display = "none";
                selectRegion.style.display = "block";
                selectRayon.style.display = "none";
            } else if (typeRecherche === "rayon") {
                inputCritere.style.display = "none";
                selectRayon.style.display = "block";
                selectRegion.style.display = "none";
            }
        }
</script>
<%
ArrayList<Federation> fe = new ArrayList<>();
ArrayList<String> re = new ArrayList<>();
ClubDAO clu = new ClubDAO();
fe=clu.findFed();
re=clu.findRegion();
%>
<form action="Traitement" method="post" id="formu">
		
        <label for="type_recherche">Type de Recherche :</label>
        <select name="type_recherche" id="type_recherche" onchange="afficherChamps()">
            <option value="code_postal">Recherche par Code Postal</option>
            <option value="region">Recherche par Région</option>
            <option value="rayon">Recherche autour de moi</option>
        </select>
        <br><br>
        <label for="federation">Type de Federation :</label><br>
        <select name="federation" id="federation">
        	<option value=""selected>Selectionnez une federation</option>
            <option value="30">Toutes les federations</option>
            <%for(int i=0 ; i<fe.size() ; i++){%>
            	<option value="<%=fe.get(i).getCode()%>"><%=fe.get(i).getFede()%></option>
           <% }%>
        </select>
        <br><br>
        <div id="critere_input">
            <label for="critere">Critère de Recherche (Code Postal) :</label>
            <input type="number" name="critere" id="critere" min="1" max="100000" step="1">
        </div>
        <div id="region_select" style="display: none;">
            <label for="region">Sélectionner une Région :</label>
            <select name="region" id="region">
                <option value="<%=re.get(0)%>"selected><%=re.get(0)%></option>
            <%for(int j=1 ; j<re.size() ; j++){%>
            	<option value="<%=re.get(j)%>"><%=re.get(j)%></option>
           <% }%>
            </select>
        </div>
         <div id="critere_rayon" style="display: none;">
            <label for="rayonn">Rayon de Recherche Max 20(km):</label>
            <input type="number" name="rayonn" id="rayonn" min="1" max="20" step="1">
        </div>
        <br>
        <input type="hidden" name="latitudeUser" id="latitudeUser">
		<input type="hidden" name="longitudeUser" id="longitudeUser">
        <input type="submit" value="Rechercher">
        
</form>
    <br><br>
                <%
                String erreur = (String) request.getAttribute("erreur");
                if (erreur != null && !erreur.isEmpty()) {%>
                    <div class="alert alert-danger" role="alert"><%=erreur %></div>
                <%}
                %>
<div id="map">
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
<script type='text/javascript' src='https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js'></script>
<script type="text/javascript">

    // Créer une carte Leaflet et la centrer sur Paris avec un zoom de 10 
    var map = L.map('map').setView([48.8566, 2.3522], 11);
  
    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
    }).addTo(map);
    
    var markerClusters = L.markerClusterGroup(); // Nous initialisons les groupes de marqueurs

    // Créer un marqueur pour une station-service et l'ajouter à la carte
    var userPosition;
    navigator.geolocation.getCurrentPosition(
        function(position) {
            userPosition = [position.coords.latitude, position.coords.longitude];
            map.setView(userPosition, 11);
            L.marker(userPosition).addTo(map)
                .bindPopup('Position actuelle') //Lie une fenêtre au marqueur avec le texte
                .openPopup(); //Ouvre automatiquement la fenêtre contextuelle lorsque la carte est chargée
            // Remplissage les champs cachés avec les coordonnées de l'utilisateur
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            document.getElementById('latitudeUser').value = lat;
            document.getElementById('longitudeUser').value = lng;
        },

        function(error) {
            console.error('Erreur de géolocalisation : ', error);
        }
    );
    var bounds = L.latLngBounds([]);
    // Créer un tableau pour stocker les positions des marqueurs
    var markerPositions = [];
    function createCircle(rayo) {
        // Obtenir la position actuelle de l'utilisateur
        navigator.geolocation.getCurrentPosition(
            function(position) {
                var userPos = [position.coords.latitude, position.coords.longitude];

                // Créer un cercle centré sur la position de l'utilisateur
                var cercle = L.circle(userPos, {
                    color: 'red',
                    fillColor: '#add8e6',
                    fillOpacity: 0.5,
                    radius: rayo * 1000 // Convertir le rayon en mètres
                }).addTo(map);

                // Ajuster le niveau de zoom pour inclure le cercle
                map.fitBounds(cercle.getBounds());
            },
            function(error) {
                console.error('Erreur de géolocalisation : ', error);
            }
        );
    }
    <% List<Commune> resultats = (List<Commune>) request.getAttribute("communes");
    List<Clubs> result = (List<Clubs>) request.getAttribute("clubs");
    String rayonn =(String) request.getAttribute("rayon");
    if (resultats != null && !resultats.isEmpty()) {%>
    var rayonnn = "<%=rayonn%>";
    //Créer un cercle autour de la position moyenne des marqueur
      if (!isNaN(rayonnn)) {
    	  var rayonnnn = parseInt(rayonnn);
    	     createCircle(rayonnnn);
    	 }
      <%  for (int k=0;k<resultats.size();k++) {
            
 %>
 var marker = L.marker([<%= resultats.get(k).getLatitude() %>, <%= resultats.get(k).getLongitude() %>])
    .bindPopup("Commune : <b> <%= resultats.get(k).getNomCommune() %> <b> <br>Code Postal : <%= resultats.get(k).getCodePostal() %> <br> Federation : <%= result.get(k).getNom_federation() %> <br> Region : <%= resultats.get(k).getRegionName() %><br> Total_Club: <%= result.get(k).getTotal_club() %> dont <br><%= result.get(k).getEpa()%> Epa <br> <%= result.get(k).getClub()%> Club <br> Licencié: <%= resultats.get(k).getRegionGeoJsonName() %> dont <br> <%= resultats.get(k).getFemm() %> Femmes <br> <%= resultats.get(k).getHomm() %> Hommes <br>Puis <%= resultats.get(k).getLabel() %> Non Renseigné");
markerClusters.addLayer(marker);
bounds.extend([<%= resultats.get(k).getLatitude() %>, <%= resultats.get(k).getLongitude() %>]);
<%
      }
      
     
  }
%>
//Attendre que les marqueurs soient ajoutés avant d'appeler map.fitBounds(bounds)
setTimeout(function() {
    // Ajouter le groupe de clusters à la carte
    map.addLayer(markerClusters);

    // Ajuster le niveau de zoom et la position de la carte pour inclure tous les marqueurs
    map.fitBounds(bounds);
}, 1000); // Changer le délai en fonction de la quantité de marqueurs ajoutés

</script>


</div>



</div>
</div>

</body>
</html>