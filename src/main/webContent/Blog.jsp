<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import= "controllers.*" %>
<%@ page import= "model.*" %>
<%@ page import= "dao.*" %>
<%@ page import= "java.io.IOException"%>
<%@ page import= " java.io.PrintWriter"%>
<%@ page import = "java.util.*"%>
<%@ page import = "jakarta.servlet.http.HttpSession"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<title>Gouv'Seekers</title>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />
<!-- Style CSS pour définir la hauteur de la carte -->
<style>
	   body{
	   position: relative;
	   }
	   
        body::before {
            content: '';
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('./backk.jpg');
            background-size: cover;
            background-repeat: repeat;
            filter: blur(7px);
            z-index: -1;
        }
  .content {
            position: relative;
            z-index: 1;
            padding: 20px;
            
            text-align: center; /* Centrer le contenu horizontalement */
        }
    .container {
        background: rgba(255, 255, 255, 0.8); /* Fond semi-transparent pour le formulaire */
        padding: 20px;
        border-radius: 10px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        max-width: 500px;
        width: 100%;
    }
    h1 {
        text-align: center;
        color: #333;
    }
    label {
        display: block;
        margin: 10px 0 5px;
        font-weight: bold;
        color: #333;
    }
    input[type="text"], input[type="file"], textarea {
        width: 100%;
        padding: 10px;
        margin-bottom: 15px;
        border: 1px solid #ccc;
        border-radius: 5px;
        box-sizing: border-box;
    }
    input[type="submit"] {
        width: 100%;
        padding: 10px;
        background-color: #4CAF50;
        border: none;
        border-radius: 5px;
        color: white;
        font-weight: bold;
        cursor: pointer;
        transition: background-color 0.3s;
    }
    input[type="submit"]:hover {
        background-color: #45a049;
    }
    .notification {
        background-color: #4CAF50;
        color: white;
        padding: 10px;
        border-radius: 5px;
        margin-bottom: 20px;
        text-align: center;
        display: none; /* Caché par défaut */
    }
</style>
<script>
    window.onload = function() {
        if (new URLSearchParams(window.location.search).has('success')) {
            document.getElementById('successNotification').style.display = 'block';
            setTimeout(function() {
                document.getElementById('successNotification').style.display = 'none';
            }, 3000); // La notification disparaît après 3 secondes
        }
    }
</script>
</head>
<body>
<nav class="navbar bg-body-tertiary">
    <a class="navbar-brand btn btn-outline-secondary" href="index.jsp">Accueil</a>
     <%
    HttpSession sessi = request.getSession(false);
     String mail="";
    if (sessi != null) {
        Utilisateur nome = (Utilisateur) sessi.getAttribute("userName");
        if (nome != null) {
        	mail=nome.getE_mail();
            String pro = nome.getprofil();
            if (!pro.equals("Lambda")) {
    %>
                <a class="btn btn-outline-primary" href="NewFile.jsp">Statistiques</a>
                <a class="btn btn-outline-primary" href="Success.jsp">Mes Posts</a>
                <a class="btn btn-outline-danger" href="Logout">Deconnexion</a>
    <%
            }
            else {
                response.sendRedirect("Logout"); // Redirige vers la page d'accueil
             }
            
        }
        else {
                       response.sendRedirect("Logout"); // Redirige vers la page d'accueil
                    }
    }
   
    %>
</nav>

    <div class="container">
        <h1>Créer un nouvel événement</h1>
        
        <div id="successNotification" class="notification">Événement créé avec succès !</div>
        
        <form action="CreateEvents" method="post" enctype="multipart/form-data">
            <label for="titre">Titre :</label>
            <input type="text" id="titre" name="titre" required>
            
            <label for="contenu">Contenu :</label>
            <textarea id="contenu" name="contenu" rows="10" cols="50" required></textarea>
            
            <label for="image">Image :</label>
            <input type="file" id="image" name="image">
            
            
            <input type="hidden" id="auteur" name="auteur" value="<%=mail %>" required>
            
            <input type="submit" value="Créer l'événement">
        </form>
    </div>
</body>
</html>
