<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="controllers.*"%>
<%@ page import="model.*"%>
<%@ page import="dao.*"%>
<%@ page import="java.io.IOException"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.util.*"%>
<%@ page import="jakarta.servlet.http.HttpSession"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js"></script>
<style>
    body {
        position: relative;
    }
    body::before {
        content: '';
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: lavender;
        background-repeat: repeat;
        filter: blur(7px);
        z-index: -1;
    }
    .content {
        position: relative;
        z-index: 1;
        padding: 20px;
        text-align: center;
    }
</style>
<title>Gouv'Seekers</title>
</head>
<body>
<nav class="navbar bg-body-tertiary">
<a class="navbar-brand btn btn-outline-secondary" href="index.jsp">Accueil</a>
<%
    HttpSession sessi = request.getSession(false);
    String mail = "";
    if (sessi != null) {
        Utilisateur nome = (Utilisateur) sessi.getAttribute("userName");
        if (nome != null) {
            mail = nome.getE_mail();
            String pro = nome.getprofil();
            if (!pro.equals("Lambda")) {
%>
<a class="btn btn-outline-primary" href="NewFile.jsp">Statistiques</a>
<a class="btn btn-outline-primary" href="Success.jsp">Mes Posts</a>
<a class="btn btn-outline-danger" href="Logout">Deconnexion</a>
<%
            } else {
                response.sendRedirect("Logout");
            }
        } else {
            response.sendRedirect("Logout");
        }
    }
%>
</nav>
<div class="container">
<div class="content">
<button class="btn btn-primary" onclick="exportPageToPDF()">Export to PDF</button>
<br><br>
</div>
<div id="page-content">
<iframe id="powerbi-iframe" title="Club Sport" width="1140" height="541.25" src="https://app.powerbi.com/reportEmbed?reportId=49dc947f-4636-4e0a-b658-a5c6676eeb1e&autoAuth=true&ctid=371cb156-9558-4286-a3cd-3059699b890c" frameborder="0" allowFullScreen="true"></iframe>
</div>
</div>
<script>
    async function exportPageToPDF() {
        const { jsPDF } = window.jspdf;
        const doc = new jsPDF();
        const pageContent = document.getElementById('page-content');
        html2canvas(pageContent).then(canvas => {
            const imgData = canvas.toDataURL('image/png');
            const imgWidth = 210; // A4 width in mm
            const pageHeight = 295; // A4 height in mm
            const imgHeight = canvas.height * imgWidth / canvas.width;
            let heightLeft = imgHeight;
            let position = 0;
 
            doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
            heightLeft -= pageHeight;
 
            while (heightLeft >= 0) {
                position = heightLeft - imgHeight;
                doc.addPage();
                doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;
            }
 
            doc.save('export.pdf');
        });
    }
</script>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
</body>
</html>