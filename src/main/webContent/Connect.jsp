<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="controllers.*, model.*, dao.*" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />
    <title>Gouv'Seekers</title>
    <style>
        body {
            position: relative;
        }
        body::before {
            content: '';
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('./backk.jpg');
            background-size: cover;
            background-repeat: repeat;
            filter: blur(7px);
            z-index: -1;
        }
        .content {
            position: relative;
            z-index: 1;
            padding: 20px;
            text-align: center;
        }
        .content-container {
            text-align: center;
            margin-top: 5px;
        }
        button {
            width: 150px;
            height: 50px;
            font-size: 16px;
            margin: 10px;
            cursor: pointer;
        }
        img {
            max-width: 70%;
            height: auto;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
<nav class="navbar bg-body-tertiary">
    <a class="navbar-brand btn btn-outline-secondary" href="index.jsp">Accueil</a>
    <%
    HttpSession sessi = request.getSession(false);
    if (sessi != null) {
        Utilisateur nome = (Utilisateur) sessi.getAttribute("userName");
        if (nome != null) {
            String pro = nome.getprofil();
            if (!pro.equals("Lambda")) {
    %>
                <a class="btn btn-outline-primary" href="NewFile.jsp">Statistiques</a>
                <a class="btn btn-outline-primary" href="Success.jsp">Mes Posts</a>
                <a class="btn btn-outline-danger" href="Logout">Deconnexion</a>
    <%
            }
            else{
            	 %>
                 <a class="btn btn-outline-primary" href="Actualite.jsp">Actualite</a>
                       <a class="btn btn-outline-danger" href="Logout">Deconnexion</a>
<%  
            }
        }
        else {
            %>
                        <a class="btn btn-outline-primary" href="Actualite.jsp">Actualite</a>
                        <a class="btn btn-outline-success" href="Connect.jsp">Compte</a>
            <%
                    }
    }
   
    %>
</nav>

<div class="content-container container">
    <h1>Bienvenue sur notre site!</h1>
    <form action="index.jsp" method="get">
        <button type="submit">Visiter notre site</button>
    </form>
    <form action="Connexion.jsp" method="get">
        <button type="submit">Se connecter</button>
    </form>
    <form action="Inscription.jsp" method="get" style="display: inline;">
        <button type="submit">Inscription</button>
    </form>
    <div class="text-center">
        <img src="7.avif" alt="Description de l'image">
    </div>
</div>
</body>
</html>
