<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import= "controllers.*" %>
<%@ page import= "model.*" %>
<%@ page import= "dao.*" %>
<%@ page import= "java.io.IOException"%>
<%@ page import= " java.io.PrintWriter"%>
<%@ page import = "java.util.*"%>
<%@ page import = "jakarta.servlet.http.HttpSession"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />

<title>Gouv'Seekers</title>
<style>
        body {
            position: relative;
        }
        body::before {
            content: '';
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('./backk.jpg');
            background-size: cover;
            background-repeat: repeat;
            filter: blur(7px);
            z-index: -1;
        }
        .content {
            position: relative;
            z-index: 1;
            padding: 20px;
            text-align: center;
        }
        .container {
            width: 80%;
            max-width: 1200px;
            margin: 20px auto;
        }
        .btn-home {
            display: inline-block;
            margin-bottom: 20px;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            text-align: center;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            text-decoration: none;
            transition: background-color 0.3s;
        }
        .btn-home:hover {
            background-color: #0056b3;
        }
        .notification {
            background-color: #dff0d8;
            color: #3c763d;
            padding: 10px;
            margin-bottom: 15px;
            border: 1px solid #d6e9c6;
            border-radius: 4px;
            position: relative;
        }
        .close-btn {
            position: absolute;
            top: 10px;
            right: 10px;
            cursor: pointer;
        }
        .event-container {
            margin-bottom: 20px;
            padding: 20px;
            border: 1px solid #ddd;
            border-radius: 5px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        .event-image {
            max-width: 200px;
            max-height: 200px;
            margin-right: 20px;
        }
        .event-details {
            flex-grow: 1;
        }
        .event-details h2 {
            margin-top: 0;
        }
        .event-actions {
            display: flex;
            gap: 10px;
            align-items: center;
        }
        .event-actions button {
            background: none;
            border: none;
            cursor: pointer;
            display: flex;
            align-items: center;
        }
        .event-actions button img {
            width: 24px;
            height: 24px;
        }
        .event-actions button span {
            margin-left: 5px;
        }
        .search-form {
            display: flex;
            justify-content: center;
            margin-bottom: 20px;
        }
        .search-form input[type="text"] {
            padding: 10px;
            width: 300px;
            border: 1px solid #ccc;
            border-radius: 4px 0 0 4px;
        }
        .search-form button {
            padding: 10px;
            background-color: #007bff;
            color: white;
            border: none;
            border-radius: 0 4px 4px 0;
            cursor: pointer;
        }
        .comment-container {
            width: 100%;
            margin-top: 20px;
        }
        .comment {
            border-top: 1px solid #ddd;
            padding: 10px 0;
        }
        .comment-form {
            margin-top: 20px;
            display: flex;
            flex-direction: column;
        }
        .comment-form textarea {
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            resize: vertical;
            min-height: 50px;
        }
        .comment-form button {
            margin-top: 10px;
            padding: 10px;
            background-color: #007bff;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
</style>
</head>
<body>
<script>
    function likeEvent(IdEvenement) {
        console.log("Liking event with ID:", IdEvenement);
        const xhr = new XMLHttpRequest();
        xhr.open("POST", "LikeServlet", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                console.log("XHR State:", xhr.readyState, "Status:", xhr.status);
                if (xhr.status === 200) {
                    const response = JSON.parse(xhr.responseText);
                    if (response.success) {
                        document.getElementById('like-count-' + IdEvenement).innerText = response.likes;
                    } else {
                        showModal(response.message);
                    }
                } else {
                    console.error("Error with request:", xhr.responseText);
                }
            }
        };
        xhr.send("IdEvenement=" + IdEvenement);
    }
 
    function commentEvent(IdEvenement) {
        console.log("Commenting on event with ID:", IdEvenement);
        const comment = prompt("Entrez votre commentaire:");
        if (comment) {
            const xhr = new XMLHttpRequest();
            xhr.open("POST", "CommentServlet", true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    console.log("XHR State:", xhr.readyState, "Status:", xhr.status);
                    if (xhr.status === 200) {
                        console.log(xhr.responseText);
                        const response = JSON.parse(xhr.responseText);
                        if (response[0].success) {
                            const commentsDiv = document.getElementById('comments-' + IdEvenement);
                            const commentElem = document.createElement('div');
                            document.getElementById('comment-count-' + IdEvenement).innerText = response[0].comment;
                            commentElem.innerText = comment;
                            commentsDiv.appendChild(commentElem);
                        } else {
                            showModal(response[0].message);
                        }
                    } else {
                        console.error("Error with request:", xhr.responseText);
                    }
                }
            };
            xhr.send("IdEvenement=" + IdEvenement + "&comment=" + encodeURIComponent(comment));
        }
    }
 
    function showModal(message) {
        $('#modalMessage').text(message);
        $('#messageModal').modal('show');
    }
    
    function showComments(IdEvenement) {
        const commentsDiv = document.getElementById('comments-' + IdEvenement);
        if (commentsDiv.style.display === 'none') {
            commentsDiv.style.display = 'block';
        } else {
            commentsDiv.style.display = 'none';
        }
    }

    function submitComment(IdEvenement) {
        const commentTextarea = document.getElementById('comment-textarea-' + IdEvenement);
        const comment = commentTextarea.value;
        if (comment) {
            const xhr = new XMLHttpRequest();
            xhr.open("POST", "CommentServlet", true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    console.log("XHR State:", xhr.readyState, "Status:", xhr.status);
                    if (xhr.status === 200) {
                        const response = JSON.parse(xhr.responseText);
                        if (response[0].success) {
                            const commentsList = document.getElementById('comments-list-' + IdEvenement);
                            const commentElem = document.createElement('div');
                            commentElem.className = 'comment';
                            commentElem.innerHTML = `<strong>${response.username}:</strong> ${comment}`;
                            commentsList.appendChild(commentElem);
                            commentTextarea.value = '';
                        } else {
                        	showModal(response[0].message);
                        }
                    } else {
                        console.error("Error with request:", xhr.responseText);
                    }
                }
            };
            xhr.send("IdEvenement=" + IdEvenement + "&comment=" + encodeURIComponent(comment));
        }
    }
</script>
 
<nav class="navbar bg-body-tertiary">
<a class="navbar-brand btn btn-outline-secondary" href="index.jsp">Accueil</a>
<%
    HttpSession sessi = request.getSession(false);
    String mail = "";
    if (sessi != null) {
        Utilisateur nome = (Utilisateur) sessi.getAttribute("userName");
        if (nome != null) {
            mail = nome.getE_mail();
            String pro = nome.getprofil();
            if (!pro.equals("Lambda")) {
                response.sendRedirect("Logout"); // Redirige vers la page d'accueil
            } else {
                %>
<a class="btn btn-outline-primary" href="Actualite.jsp">Actualite</a>
<a class="btn btn-outline-danger" href="Logout">Deconnexion</a>
<%
            }
        } else {
            %>
<a class="btn btn-outline-primary" href="Actualite.jsp">Actualite</a>
<a class="btn btn-outline-success" href="Connect.jsp">Compte</a>
<%
        }
    }
    %>
</nav>
 
<div class="container content">

<div id="liveAlertPlaceholder"></div>
<% String notification = (String) request.getAttribute("notification"); %>
<% String notificationModification = (String) request.getAttribute("notificationModification"); %>
<% if (notification != null && !notification.isEmpty()) { %>
<div class="alert alert-success alert-dismissible" role="alert">
<%= notification %>
<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
<% } %>
<% if (notificationModification != null && !notificationModification.isEmpty()) { %>
<div class="alert alert-info alert-dismissible" role="alert">
<%= notificationModification %>
<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
<% } %>
 
    <%
    List<Evenement> eventsList = new ArrayList<>();
    EvenementDAO evenementDAO = new EvenementDAO();
    eventsList = evenementDAO.getList();
 
    if (eventsList != null && !eventsList.isEmpty()) {
        for (Evenement event : eventsList) {
            int likeCount = evenementDAO.getLikeCount(event.getIdEvenement());
            int commentCount = evenementDAO.getcommentCount(event.getIdEvenement());
            List<Commentaire> commentaires = evenementDAO.getCommentaires(event.getIdEvenement());
    %>
 <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="messageModalLabel">Notification</h5>
            </div>
            <div class="modal-body" id="modalMessage">
            </div>
            
        </div>
    </div>
</div>
<div class="event-container">
		<% if (event.getImage() != null) { %>
		<img src="data:image/jpeg;base64,<%= org.apache.tomcat.util.codec.binary.Base64.encodeBase64String(event.getImage()) %>" class="event-image" alt="<%= event.getTitre() %>">
		<% } %>
		<div class="event-details">
			<h2><%= event.getTitre() %></h2>
			<p><%= event.getContenu() %></p>
			<p>Auteur : <%= evenementDAO.getNom(event.getAuteur()) %></p>
		</div>
		<div class="event-actions">
			<button onclick="likeEvent(<%= event.getIdEvenement() %>)">
			<img src="ressources/like-icon.png" alt="Like">
			<span id="like-count-<%= event.getIdEvenement() %>"><%= likeCount %></span>
			</button>
			<button onclick="showComments(<%= event.getIdEvenement() %>)">
			<img src="ressources/comment-icon.jpeg" alt="Comment">
			<span id="comment-count-<%= event.getIdEvenement() %>"> <%= commentCount %></span>
			</button>
		</div>
</div>
<div id="comments-<%= event.getIdEvenement() %>" class="comment-container content" style="display: none;">
	<div id="comments-list-<%= event.getIdEvenement() %>">
	<% for (Commentaire commentaire : commentaires) { %>
		<div class="comment">
		<strong><%= evenementDAO.getNom(commentaire.getIdUtilisateur()) %>:</strong> <%= commentaire.getContenu() %>
		</div>
	<% } %>
</div>
	<div class="comment-form">
		<textarea id="comment-textarea-<%= event.getIdEvenement() %>" placeholder="Écrire un commentaire..."></textarea>
		<button onclick="submitComment(<%= event.getIdEvenement() %>)">Envoyer</button>
	</div>
	</div>

 
    <%
        }
    } else {
    %>
<div class="alert alert-danger" role="alert">
        Aucun Événement Posté <a class="btn btn-success" href="index.jsp">Retour</a>
</div>
<%
    }
    %>
    </div>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
</body>
</html>