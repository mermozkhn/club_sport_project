<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import= "controllers.*" %>
<%@ page import= "model.*" %>
<%@ page import= "dao.*" %>
<%@ page import= "java.io.IOException"%>
<%@ page import= "java.util.*"%>
<%@ page import= "jakarta.servlet.http.HttpSession"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.3/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
<title>Gouv'Seekers</title>
<style>
    body {
        position: relative;
    }
 
    body::before {
        content: '';
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-image: url('./backk.jpg');
        background-size: cover;
        background-repeat: repeat;
        filter: blur(7px);
        z-index: -1;
    }
 
    .content {
        position: relative;
        z-index: 1;
        padding: 20px;
        text-align: center;
    }
 
    #chart-male {
        position: relative;
        z-index: 1;
        text-align: center;
        background-color: lavender;
    }
 
    #chart-female {
        position: relative;
        z-index: 1;
        text-align: center;
        background-color: lavender;
    }
</style>
</head>
<body>
<nav class="navbar bg-body-tertiary">
<a class="navbar-brand btn btn-outline-secondary" href="index.jsp">Accueil</a>
<%
    HttpSession sessi = request.getSession(false);
     String mail="";
    if (sessi != null) {
        Utilisateur nome = (Utilisateur) sessi.getAttribute("userName");
        if (nome != null) {
            mail = nome.getE_mail();
            String pro = nome.getprofil();
            if (!pro.equals("Lambda")) {
    %>
<a class="btn btn-outline-primary" href="NewFile.jsp">Statistiques</a>
<a class="btn btn-outline-primary" href="Success.jsp">Mes Posts</a>
<a class="btn btn-outline-danger" href="Logout">Deconnexion</a>
<%
            } 
        }
    }
%>
</nav>
<div class="container">
<div class="content text-center">
 
<h1>Bienvenue sur notre site</h1>
<script>
    function afficherChamp() {
        var typeRecherche = document.getElementById("type_recherchee").value;
        var inputCritere = document.getElementById("critere_input");
        var selectRegion = document.getElementById("region_select");
        var selectRayon = document.getElementById("critere_dep");
 
        inputCritere.style.display = "none";
        selectRegion.style.display = "none";
        selectRayon.style.display = "none";
 
        if (typeRecherche === "region") {
            selectRegion.style.display = "block";
        } else if (typeRecherche === "commune") {
            inputCritere.style.display = "block";
        } else if (typeRecherche === "departement") {
            selectRayon.style.display = "block";
        }
    }
</script>
 
<%
ArrayList<Federation> fe = new ArrayList<>();
ArrayList<String> re = new ArrayList<>();
ArrayList<String> co = new ArrayList<>();
ArrayList<String> de = new ArrayList<>();
ClubDAO clu = new ClubDAO();
fe = clu.findFed();
re = clu.findRegion();
de = clu.findDep();
co = clu.findCom();
%>
 
<form action="Treat" method="post" id="formulai">
<label for="type_recherchee">Filtre :</label>
<select id="type_recherchee" name="type_recherchee" onchange="afficherChamp()">
<option value="region">Par région</option>
<option value="departement">Par département</option>
<option value="commune">Par commune</option>
</select>
<br>
<label for="federation">Type de Federation :</label><br>
<select name="federation" id="federation">
<option value="" selected>Selectionnez une federation</option>
<% for (int i = 0; i < fe.size(); i++) { %>
<option value="<%= fe.get(i).getCode() %>"><%= fe.get(i).getFede() %></option>
<% } %>
</select>
<br>
<div id="critere_input" style="display: none;">
<label for="commune">Sélectionner une Commune :</label>
<input type="text" id="search" name="commune" class="form-control">
</div>
<br>
<div id="region_select">
<label for="region">Sélectionner une Région :</label>
<select name="region" id="region">
<option value="<%= re.get(0) %>" selected><%= re.get(0) %></option>
<% for (int j = 1; j < re.size(); j++) { %>
<option value="<%= re.get(j) %>"><%= re.get(j) %></option>
<% } %>
</select>
</div>
<br><br>
<div id="critere_dep" style="display: none;">
<label for="departement">Sélectionner un Departement :</label>
<select name="departement" id="departement">
<option value="<%= de.get(0) %>" selected><%= de.get(0) %></option>
<% for (int j = 1; j < de.size(); j++) { %>
<option value="<%= de.get(j) %>"><%= de.get(j) %></option>
<% } %>
</select>
</div>
<br>
<input type="submit" value="Rechercher">
</form>
<br><br>
 
<!-- Conteneur des deux diagrammes -->
<div id="charts-container">
<!-- Diagramme pour les hommes -->
<canvas id="chart-male" width="400" height="200"></canvas>
<!-- Diagramme pour les femmes -->
<canvas id="chart-female" width="400" height="200"></canvas>
</div>
 <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
 <script src="https://code.jquery.com/jquery-3.6.0.min.js" defer></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" defer></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.3/js/bootstrap.bundle.min.js" defer></script>

<script>
    // Gestionnaire d'événements pour le soumission du formulaire
<% String jso = (String) request.getAttribute("club");
    if (jso != null && !jso.isEmpty()) { %>
        var dat = '<%= jso %>';
        var data = JSON.parse(dat);
        console.log(data);
        drawCharts(data);
<% } %>
 
    // Fonction pour dessiner les diagrammes à partir des données récupérées
    function drawCharts(data) {
        const maleData = data.homme;
        const femaleData = data.femme;
        const ages = Object.keys(maleData);
        const maleValues = Object.values(maleData);
        const femaleValues = Object.values(femaleData);
 
        const ctxMale = document.getElementById('chart-male').getContext('2d');
        const chartMale = new Chart(ctxMale, {
            type: 'bar',
            data: {
                labels: ages,
                datasets: [{
                    label: 'Hommes',
                    data: maleValues,
                    backgroundColor: 'rgba(54, 162, 235, 0.5)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
 
        const ctxFemale = document.getElementById('chart-female').getContext('2d');
        const chartFemale = new Chart(ctxFemale, {
            type: 'bar',
            data: {
                labels: ages,
                datasets: [{
                    label: 'Femmes',
                    data: femaleValues,
                    backgroundColor: 'rgba(255, 99, 132, 0.5)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    }
</script>
</div>
</div>
<script>
    $(document).ready(function() {
        $('#search').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: 'Suggestion',
                    dataType: 'json',
                    data: {
                        keyword: request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            }
        });
 
        // Empêcher la soumission du formulaire lorsque la touche "Entrée" est pressée
        $('#search').keydown(function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                return false;
            }
        });
    });
</script>

</body>
</html>