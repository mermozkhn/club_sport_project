<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import= "controllers.*" %>
<%@ page import= "model.*" %>
<%@ page import= "dao.*" %>
<%@ page import= "java.io.IOException"%>
<%@ page import= " java.io.PrintWriter"%>
<%@ page import = "java.util.*"%>

<!DOCTYPE html>
<html>
<head>
 <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<title>Gouv'Seekers</title>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />
<!-- Style CSS pour définir la hauteur de la carte -->
<style>
	   body{
	   position: relative;
	   }
	   
        body::before {
            content: '';
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('./backk.jpg');
            background-size: cover;
            background-repeat: repeat;
            filter: blur(7px);
            z-index: -1;
        }
  .content {
            position: relative;
            z-index: 1;
            padding: 20px;
            
            text-align: center; /* Centrer le contenu horizontalement */
        }
        }
        form {
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        div {
            margin-bottom: 10px;
        }
        label {
            display: block;
            margin-bottom: 5px;
        }
        input[type="email"], input[type="password"], select {
            width: calc(100% - 22px);
            padding: 10px;
            border: 1px solid #ddd;
            border-radius: 4px;
        }
        button {
            background-color: #007BFF;
            color: white;
            padding: 10px 15px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        button:hover {
            background-color: #0056b3;
        }
         .return-section {
            text-align: center;
        }
          .return-section img {
            display: block;
            margin: 10px auto;
        }
    </style>
</head>
<body>
<nav class="navbar bg-body-tertiary">
    <a class="navbar-brand btn btn-outline-secondary" href="index.jsp">Accueil</a>
     <%
    HttpSession sessi = request.getSession(false);
     String mail="";
    if (sessi != null) {
        Utilisateur nome = (Utilisateur) sessi.getAttribute("userName");
        if (nome != null) {
        	mail=nome.getE_mail();
            String pro = nome.getprofil();
            if (!pro.equals("")) {
            	response.sendRedirect("Logout"); // Redirige vers la page d'accueil
            } 
        }
        else {%>
    	<a class="btn btn-outline-primary" href="Actualite.jsp">Actualite</a>
        <a class="btn btn-outline-success" href="Connect.jsp">Compte</a>
    	<% 	}
    }
   
    %>
</nav>
  <div class="container content">
    <h2>Connexion</h2>
     <%
                String erreur = (String) request.getAttribute("errorMessage");
                if (erreur != null && !erreur.isEmpty()) {%>
                    <div class="alert alert-danger" role="alert"><%=erreur %></div>
                <%}
                %>
    <form action="Connection" method="post">
        <div>
            <label for="email">E-Mail :</label>
            <input type="email" id="email" name="email" required>
        </div>
        <div>
            <label for="password">Mot de passe :</label>
            <input type="password" id="password" name="password" required>
        </div>
        <div>
            <button type="submit">Se connecter</button>
        </div>
    </form>
     <div class="return-section">
        <button onclick="window.location.href='Connect.jsp'">Retour</button>
        <img src="2.png">
    </div>
    </div>
    	
</body>
</html>
