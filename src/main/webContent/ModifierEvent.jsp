<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="model.*" %>
<%@ page import="dao.*" %>
<%@ page import="controllers.*" %>


<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
    <style>
      body{
	   position: relative;
	   }
	   
        body::before {
            content: '';
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('./backk.jpg');
            background-size: cover;
            background-repeat: repeat;
            filter: blur(7px);
            z-index: -1;
        }
  .content {
            position: relative;
            z-index: 1;
            padding: 20px;
            text-align: center; /* Centrer le contenu horizontalement */
        }
       
        .form-container {
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            width: 100%%;
            max-width: 100%;
        }
        .form-container h1 {
            text-align: center;
        }
        .form-container label {
            display: block;
            margin-top: 10px;
        }
        .form-container input[type="text"],
        .form-container textarea {
            width: 100%;
            padding: 10px;
            margin-top: 5px;
            border: 1px solid #ddd;
            border-radius: 4px;
            box-sizing: border-box;
        }
        .form-container input[type="submit"] {
            display: block;
            width: 100%;
            padding: 10px;
            margin-top: 20px;
            background-color: #28a745;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
        }
        .form-container input[type="submit"]:hover {
            background-color: #218838;
        }
    </style>
</head>
<body>
<nav class="navbar bg-body-tertiary">
    <a class="navbar-brand btn btn-outline-secondary" href="index.jsp">Accueil</a>
     <%
    HttpSession sessi = request.getSession(false);
     String mail="";
    if (sessi != null) {
        Utilisateur nome = (Utilisateur) sessi.getAttribute("userName");
        if (nome != null) {
        	mail=nome.getE_mail();
            String pro = nome.getprofil();
            if (!pro.equals("Lambda")) {
    %>
                <a class="btn btn-outline-primary" href="NewFile.jsp">Statistiques</a>
                <a class="btn btn-outline-primary" href="Success.jsp">Mes Posts</a>
                <a class="btn btn-outline-danger" href="Logout">Deconnexion</a>
    <%
            }
            else {
                response.sendRedirect("Logout"); // Redirige vers la page d'accueil
             }
            
        }
        else {
                       response.sendRedirect("Logout"); // Redirige vers la page d'accueil
                    }
    }
   
    %>
</nav>
<%

    // Récupération de l'identifiant de l'événement depuis les paramètres de la requête
    String idEvenementStr = request.getParameter("id");
    int idEvenement = Integer.parseInt(idEvenementStr);
    
    // Création d'une instance de EvenementDAO
    EvenementDAO evenementDAO = new EvenementDAO();
    
    // Récupération de l'événement depuis la base de données en utilisant l'identifiant
    Evenement event = evenementDAO.get(idEvenement);
%>
<div class="container content">
<div class="form-container">
    <h1>Modifier l'événement</h1>
    <form action="ModifierEvent" method="post" enctype="multipart/form-data">
        <!-- Champ caché pour transmettre l'identifiant de l'événement -->
        <input type="hidden" name="idEvenement" value="<%= idEvenement %>">
        
        <label for="titre">Titre :</label>
        <input type="text" id="titre" name="titre" value="<%= event.getTitre() %>" required>
        
        <label for="contenu">Contenu :</label>
        <textarea id="contenu" name="contenu" rows="10" cols="50" required><%= event.getContenu() %></textarea>
        
       
        <input type="hidden" id="auteur" name="auteur" value="<%= event.getAuteur() %>" required>
        
        <label for="image">Image :</label>
        <input type="file" id="image" name="image" accept="image/*">
        
        <input type="submit" value="Modifier l'événement">
    </form>
</div>
  </div>
</body>
</html>