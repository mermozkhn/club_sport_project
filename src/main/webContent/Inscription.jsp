<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import= "controllers.*" %>
<%@ page import= "model.*" %>
<%@ page import= "dao.*" %>
<%@ page import= "java.util.*"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<title>Gouv'Seekers</title>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />
<!-- Style CSS pour définir la hauteur de la carte -->
<style>
	   body{
	   position: relative;
	   }
	   
        body::before {
            content: '';
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('./backk.jpg');
            background-size: cover;
            background-repeat: repeat;
            filter: blur(7px);
            z-index: -1;
        }
  .content {
            position: relative;
            z-index: 1;
            padding: 20px;
            
            text-align: center; /* Centrer le contenu horizontalement */
        }
        #map {
            position: relative;
            z-index: 1;
            padding: 20px;
            height: 500px;
            text-align: center; /* Centrer le contenu horizontalement */
        }
        form {
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        div {
            margin-bottom: 10px;
        }
        label {
            display: block;
            margin-bottom: 5px;
        }
        input[type="text"], input[type="email"], input[type="password"], input[type="file"], select {
            width: calc(100% - 22px);
            padding: 10px;
            border: 1px solid #ddd;
            border-radius: 4px;
        }
        button {
            background-color: #007BFF;
            color: white;
            padding: 10px 15px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        button:hover {
            background-color: #0056b3;
        }
        .return-section {
            text-align: center;
        }
        .return-section img {
            display: block;
            margin: 10px auto;
        }
    </style>
    <script>
        function clearFileInput() {
            var fileInput = document.getElementById('file-upload');
            fileInput.value = ''; // Clear the file input
        }
        function afficherChamps() {
            var typeRecherche = document.getElementById("fonction").value;
            var selectCommune = document.getElementById("commune_select");
            var selectRegion = document.getElementById("region_select");
            var selectFede = document.getElementById("fede_select");
            var selectFile = document.getElementById("file_select");

            if (typeRecherche === "Elus") {
                selectCommune.style.display = "none";
                selectRegion.style.display = "block";
                selectFede.style.display = "none";
                selectFile.style.display = "block";
            } else if (typeRecherche === "President") {
                selectCommune.style.display = "block";
                selectFede.style.display = "block";
                selectRegion.style.display = "none";
                selectFile.style.display = "block";
            } else if (typeRecherche === "Trainer") {
                selectCommune.style.display = "block";
                selectFede.style.display = "block";
                selectRegion.style.display = "none";
                selectFile.style.display = "block";
            } else if (typeRecherche === "Lambda") {
                selectCommune.style.display = "none";
                selectFede.style.display = "none";
                selectRegion.style.display = "none";
                selectFile.style.display = "none";
            }
        }
    </script>
</head>
<body>
<nav class="navbar bg-body-tertiary">
    <a class="navbar-brand btn btn-outline-secondary" href="index.jsp">Accueil</a>
     <%
    HttpSession sessi = request.getSession(false);
     String mail="";
    if (sessi != null) {
        Utilisateur nome = (Utilisateur) sessi.getAttribute("userName");
        if (nome != null) {
        	mail=nome.getE_mail();
            String pro = nome.getprofil();
            if (!pro.equals("")) {
            	response.sendRedirect("Logout"); // Redirige vers la page d'accueil
            } 
        }
        else {%>
    	<a class="btn btn-outline-primary" href="Actualite.jsp">Actualite</a>
        <a class="btn btn-outline-success" href="Connect.jsp">Compte</a>
    	<% 	}
    }
   
    %>
</nav>
 <div class="container content">
    <h2>Inscription</h2>
<%
    ArrayList<Federation> fe = new ArrayList<>();
    ArrayList<String> re = new ArrayList<>();
    ClubDAO clu = new ClubDAO();
    fe = clu.findFed();
    re = clu.findRegion();
%>
 <%
                String erreur = (String) request.getAttribute("erreur");
 				String erro = (String) request.getAttribute("errorMessage");
                if (erreur != null && !erreur.isEmpty()) {%>
                    <div class="alert alert-danger" role="alert"><%=erreur %></div>
                   
                <%}
                if (erro != null && !erro.isEmpty()) {%>
                 <div class="alert alert-danger" role="alert"><%=erro %></div>
                <%}
                %>
    <form action="Inscription" method="post" enctype="multipart/form-data">
        <div>
            <label for="nom">Nom :</label>
            <input type="text" id="nom" name="nom" required>
        </div>
        <div>
            <label for="prenom">Prénoms :</label>
            <input type="text" id="prenom" name="prenom" required>
        </div>
        <div>
            <label for="email">E_Mail :</label>
            <input type="email" id="email" name="email" required>
        </div>
        <div>
            <label for="password">Mot de passe :</label>
            <input type="password" id="motDePasse" name="motDePasse" required>
        </div>
        <div>
            <label for="fonction">Sélectionnez une fonction :</label>
            <select id="fonction" name="fonction" onchange="afficherChamps()" required>
                <option value="Elus" selected>Elus</option>
                <option value="President">President</option>
                <option value="Trainer">Entraineur</option>
                <option value="Lambda">Public</option>
            </select>
        </div>
        <div id="region_select">
            <label for="region">Sélectionner une Région :</label>
            <select name="region" id="region">
                <% for (String region : re) { %>
                    <option value="<%= region %>"><%= region %></option>
                <% } %>
            </select>
        </div>
        <div id="commune_select" style="display: none;">
            <label for="critere">Critère de Recherche (Code Postal) :</label>
            <input type="number" name="critere" id="critere" min="1" max="100000" step="1">
        </div>
        <div id="fede_select" style="display: none;">
            <label for="fede">Sélectionner une Federation :</label>
            <select name="fede" id="fede">
            <%for(int i=0 ; i<fe.size() ; i++){%>
            	<option value="<%=fe.get(i).getCode() %>"><%=fe.get(i).getFede()%></option>
           <% }%>
            </select>
        </div>
        <div id="file_select">
            <label for="file-upload">Joindre vos documents (PDF uniquement) :</label>
            <input type="file" id="file-upload" name="file-upload" accept=".pdf">
            <button type="button" onclick="clearFileInput()">Annuler la pièce jointe</button>
        </div>
        <div>
            <button type="submit">S'inscrire</button>
        </div>
    </form>
    <div class="return-section">
        <button onclick="window.location.href='Connect.jsp'">Retour</button>
        <img src="9.jpg" alt="Retour">
    </div>
    
   </div>
</body>
</html>
