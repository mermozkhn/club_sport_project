<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import= "controllers.*" %>
<%@ page import= "model.*" %>
<%@ page import= "dao.*" %>
<%@ page import= "java.io.IOException"%>
<%@ page import= " java.io.PrintWriter"%>
<%@ page import = "java.util.*"%>
<%@ page import = "jakarta.servlet.http.HttpSession"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<title>Gouv'Seekers</title>
    <style>
     body{
	   position: relative;
	   }
	   
        body::before {
            content: '';
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('./backk.jpg');
            background-size: cover;
            background-repeat: repeat;
            filter: blur(7px);
            z-index: -1;
        }
  .content {
            position: relative;
            z-index: 1;
            padding: 20px;
            
            text-align: center; /* Centrer le contenu horizontalement */
        }
        .container {
            width: 80%;
            max-width: 1200px;
            margin: 20px auto;
        }
        .btn-home {
            display: inline-block;
            margin-bottom: 20px;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            text-align: center;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            text-decoration: none;
            transition: background-color 0.3s;
        }
        .btn-home:hover {
            background-color: #0056b3;
        }
        .notification {
            background-color: #dff0d8;
            color: #3c763d;
            padding: 10px;
            margin-bottom: 15px;
            border: 1px solid #d6e9c6;
            border-radius: 4px;
            position: relative;
        }
        .close-btn {
            position: absolute;
            top: 10px;
            right: 10px;
            cursor: pointer;
        }
        .event-container {
            margin-bottom: 20px;
            padding: 20px;
            border: 1px solid #ddd;
            border-radius: 5px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        .event-image {
            max-width: 200px;
            max-height: 200px;
            margin-right: 20px;
        }
        .event-details {
            flex-grow: 1;
        }
        .event-details h2 {
            margin-top: 0;
        }
        .event-actions {
            display: flex;
            gap: 10px;
            align-items: center;
        }
        .event-actions button {
            background: none;
            border: none;
            cursor: pointer;
            display: flex;
            align-items: center;
        }
        .event-actions button img {
            width: 24px;
            height: 24px;
        }
        .event-actions button span {
            margin-left: 5px;
        }
        .search-form {
            display: flex;
            justify-content: center;
            margin-bottom: 20px;
        }
        .search-form input[type="text"] {
            padding: 10px;
            width: 300px;
            border: 1px solid #ccc;
            border-radius: 4px 0 0 4px;
        }
        .search-form button {
            padding: 10px;
            background-color: #007bff;
            color: white;
            border: none;
            border-radius: 0 4px 4px 0;
            cursor: pointer;
        }
    </style>
</head>
<body>
<nav class="navbar bg-body-tertiary">
    <a class="navbar-brand btn btn-outline-secondary" href="index.jsp">Accueil</a>
     <%
    HttpSession sessi = request.getSession(false);
     String mail="";
    if (sessi != null) {
        Utilisateur nome = (Utilisateur) sessi.getAttribute("userName");
        if (nome != null) {
        	mail=nome.getE_mail();
            String pro = nome.getprofil();
            if (!pro.equals("Lambda")) {
    %>
                <a class="btn btn-outline-primary" href="NewFile.jsp">Statistiques</a>
                <a class="btn btn-outline-primary" href="Success.jsp">Mes Posts</a>
                <a class="btn btn-outline-danger" href="Logout">Deconnexion</a>
    <%
            }
            else {
                response.sendRedirect("Logout"); // Redirige vers la page d'accueil
             }
            
        }
        else {
                       response.sendRedirect("Logout"); // Redirige vers la page d'accueil
                    }
    }
   
    %>
</nav>
<div class="container content">
    <% String notification = (String) request.getAttribute("notification"); %>
    <% String notificationModification = (String) request.getAttribute("notificationModification"); %>
    <% if (notification != null && !notification.isEmpty()) { %>
        <div class="notification">
        <span class="close-btn" onclick="this.parentElement.style.display='none';">&times;</span>
            <%= notification %>
            </div>
    <% } %>
    <% if (notificationModification != null && !notificationModification.isEmpty()) { %>
        <div class="notification" id="notification">
            <span class="close-btn" onclick="closeNotification()">&times;</span>
            <%= notificationModification %>
        </div>
    <% } %>
    <a class="btn btn-success" href="Blog.jsp">Ajouter un Evenement</a>
    
    	
<%
List<Evenement> eventsList = new ArrayList<>();
EvenementDAO evenementDAO = new EvenementDAO();%>
 <!-- Barre de recherche -->
        <form action="Success.jsp" method="get" class="search-form">
            <input type="text" id="search" name="keyword" placeholder="Rechercher un événement...">
            <button type="submit">Rechercher</button>
        </form>
         <% 
        String keyword = request.getParameter("keyword");
       
        // Si un mot-clé est spécifié, filtrer les événements
        if (keyword != null && !keyword.isEmpty()) {
            eventsList = evenementDAO.getListByKeyword(keyword);
        } else {
            eventsList = evenementDAO.getLis(mail);
        }
        %>
<%
   if (eventsList != null && !eventsList.isEmpty()) {
       for (Evenement event : eventsList) {
%>
	
        <div class="event-container">
		<% if (event.getImage() != null) { %>
		<img src="data:image/jpeg;base64,<%= org.apache.tomcat.util.codec.binary.Base64.encodeBase64String(event.getImage()) %>" class="event-image" alt="<%= event.getTitre() %>">
		<% } %>
		<div class="event-details">
		<h2><%= event.getTitre() %></h2>
		<p><%= event.getContenu() %></p>
		</div>
            <form action="ManageEvent" method="post" style="display: inline;">
                <input type="hidden" name="idEvenement" value="<%= event.getIdEvenement() %>">
                <button type="submit" name="action" value="modifier">Modifier</button>
            </form>
            <form action="ManageEvent" method="post" style="display: inline;">
                <input type="hidden" name="idEvenement" value="<%= event.getIdEvenement() %>">
                <button type="submit" name="action" value="supprimer">Supprimer</button>
            </form>
        </div>
<%
       }
   } else {
%>
       <div class="alert alert-danger" role="alert">Aucun évenement posté à votre actif <a class="btn btn-success" href="Blog.jsp">Postez-en</a></div>
<%
   }
%>
</div>
</body>
</html>
